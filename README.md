The **Loki** library is currently in development [**Version 0.0.1**]

[[_TOC_]]

## Loki

The **Loki** Bittorrent library implements the download of **magnet** URI.

This sections describes the main goals of this project

- The library should be usable in the mobile context (Android). That requires that it is optimize
  towards memory usage and performance. The dependency on Android limits the used OpenJdk version.
  The minimum supported Android SDK version defines the OpenJdk version. Currently SDK35 with
  OpenJdk version 21 is supported [**Version 1.0.0**]
- The size of the library should be as small as possible. Reason is that it should be compiled as
  wasm library, and the more data are transferred in the Web context the less attractive the library
  will be (same goes for Android apps). The reduced size will be achieved by a well defined set of
  features and a pure kotlin implementation. [**Version 1.0.0**]
- Library is a Kotlin Multiplatform Library [**Version 1.0.0**]
- The library should be published to Maven Central [**Version 1.0.0**]

## Download Magnet URI

```
     @Test
    fun downloadMagnetUri(): Unit = runBlocking(Dispatchers.IO) {
        val uri =
            "magnet:?xt=urn:btih:54943684744B92BD30C7261A4806D9B5CA58F946&dn=..." 

        val magnetUri = parseMagnetUri(uri)

        val baseDirectory = Files.createTempDirectory("").toFile()

        val directory = File(baseDirectory, magnetUri.displayName ?: "empty")
        if (!directory.exists()) {
            val success = directory.mkdir()
            assertTrue(success)
        }

        downloadMagnetUri(
            directory,
            magnetUri,
            1000
        ) { torrentState: TorrentState, closable: AutoCloseable ->
            val completePieces = torrentState.piecesComplete
            val totalPieces = torrentState.piecesTotal

            System.err.println(" pieces : $completePieces/$totalPieces")
        }
    }
    
```