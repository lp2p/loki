package tech.lp2p.loki.protocol

class NotInterested : Message {
    override val messageId: Byte
        get() = StandardBittorrentProtocol.NOT_INTERESTED_ID
    override val type: Type
        get() = Type.NotInterested

}

private val instance = NotInterested()

fun notInterested(): NotInterested {
    return instance
}