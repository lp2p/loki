package tech.lp2p.loki.protocol

import tech.lp2p.loki.buffer.ByteBufferView

abstract class UniqueMessageHandler internal constructor(private val type: Type) :
    BaseMessageHandler() {
    override fun supportedTypes(): Collection<Type> =
        setOf(type)

    override fun readMessageType(buffer: ByteBufferView): Type {
        return type
    }
}
