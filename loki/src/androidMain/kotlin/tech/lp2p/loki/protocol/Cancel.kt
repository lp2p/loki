package tech.lp2p.loki.protocol

data class Cancel(
    val pieceIndex: Int,
    val offset: Int,
    val length: Int
) : Message {
    override val messageId: Byte
        get() = StandardBittorrentProtocol.CANCEL_ID

    override val type: Type
        get() = Type.Cancel

    init {
        require(!(pieceIndex < 0 || offset < 0 || length <= 0)) {
            "Illegal arguments: piece index (" +
                    pieceIndex + "), offset (" + offset + "), length (" + length + ")"
        }
    }
}
