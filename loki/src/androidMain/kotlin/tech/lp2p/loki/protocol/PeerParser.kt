package tech.lp2p.loki.protocol

import tech.lp2p.loki.net.Peer
import tech.lp2p.loki.net.createPeer
import java.net.InetAddress


private fun parsePeers(
    peers: ByteArray,
    cryptoFlags: ByteArray?,
    addressLength: Int
): List<Peer> {
    var pos = 0
    var index = 0

    val result = mutableListOf<Peer>()
    while (pos < peers.size) {

        val inetAddress: InetAddress
        val port: Int

        var from = pos
        pos += addressLength
        var to = pos

        inetAddress = InetAddress.getByAddress(peers.copyOfRange(from, to))


        from = to
        pos += PORT_LENGTH
        to = pos
        port =
            (((peers[from].toInt() shl 8) and 0xFF00) + (peers[to - 1].toInt() and 0x00FF))

        var options = EncryptionPolicy.PREFER_PLAINTEXT
        val requiresEncryption = cryptoFlags != null && cryptoFlags[index].toInt() == 1
        if (requiresEncryption) {
            options = EncryptionPolicy.PREFER_ENCRYPTED
        }
        val peer: Peer = createPeer(inetAddress, port, options)
        index++
        result.add(peer)

    }
    return result
}

private const val PORT_LENGTH = 2


@Suppress("unused")
enum class AddressType(val length: Int) {
    IPV4(4),
    IPV6(16);
}

fun parsePeers(
    peers: ByteArray, addressType: AddressType,
    destination: MutableCollection<Peer>, cryptoFlags: ByteArray? = null
) {

    val peerLength = addressType.length + PORT_LENGTH
    require(peers.size % peerLength == 0) {
        "Invalid peers string (" + addressType.name + ") -- length (" +
                peers.size + ") is not divisible by " + peerLength
    }
    val numOfPeers = peers.size / peerLength
    require(!(cryptoFlags != null && cryptoFlags.size != numOfPeers)) {
        "Number of peers (" + numOfPeers +
                ") is different from the number of crypto flags (" + cryptoFlags!!.size + ")"
    }
    val addressLength = addressType.length

    destination.addAll(parsePeers(peers, cryptoFlags, addressLength))

}