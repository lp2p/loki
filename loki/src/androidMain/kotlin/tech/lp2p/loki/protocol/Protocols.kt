package tech.lp2p.loki.protocol

import tech.lp2p.loki.buffer.ByteBufferView

/**
 * Get 2-bytes binary representation of a [Short].
 */
fun getShortBytes(s: Int): ByteArray {
    return byteArrayOf(
        (s shr 8).toByte(),
        s.toByte()
    )
}

/**
 * Decode the binary representation of an [Integer] from a byte array.
 *
 * @param bytes  Arbitrary byte array.
 * It's length must be at least **offset** + 4.
 * @param offset Offset in byte array to start decoding from (inclusive, 0-based)
 */
fun readInt(bytes: ByteArray, offset: Int): Int {
    if (bytes.size < offset + Integer.BYTES) {
        throw ArrayIndexOutOfBoundsException(
            "insufficient byte array length (length: " + bytes.size +
                    ", offset: " + offset + ")"
        )
    }
    return ((bytes[offset].toInt() and 0xFF) shl 24) or
            ((bytes[offset + 1].toInt() and 0xFF) shl 16) or
            ((bytes[offset + 2].toInt() and 0xFF) shl 8) or
            (bytes[offset + 3].toInt() and 0xFF)
}


fun readInt(buffer: ByteBufferView): Int? {
    if (buffer.remaining() < Integer.BYTES) {
        return null
    }
    return buffer.int
}

/**
 * Decode the binary representation of a [Short] from a byte array.
 *
 * @param bytes  Arbitrary byte array.
 * It's length must be at least **offset** + 2.
 * @param offset Offset in byte array to start decoding from (inclusive, 0-based)
 */
fun readShort(bytes: ByteArray, offset: Int): Short {
    if (bytes.size < offset + Short.SIZE_BYTES) {
        throw ArrayIndexOutOfBoundsException(
            "insufficient byte array length (length: " + bytes.size +
                    ", offset: " + offset + ")"
        )
    }
    return (((bytes[offset].toInt() and 0xFF) shl 8) or
            ((bytes[offset + 1].toInt() and 0xFF))).toShort()
}


fun readShort(buffer: ByteBufferView): Short? {
    if (buffer.remaining() < Short.SIZE_BYTES) {
        return null
    }
    return buffer.short
}

/**
 * Convenience method to check if actual message length is the same as expected length.
 */
fun verifyPayloadHasLength(type: Type, expectedLength: Int, actualLength: Int) {
    require(expectedLength == actualLength) {
        "Unexpected payload length for " +
                type.name + ": " + actualLength + " (expected " + expectedLength + ")"
    }
}

/**
 * Sets i-th bit in a bitmask.
 *
 * @param bytes    Bitmask.
 * @param bitOrder Order of bits in a byte
 * @param i        Bit index (0-based)
 */
fun setBit(bytes: ByteArray, bitOrder: BitOrder, i: Int) {
    val byteIndex = (i / 8.0).toInt()
    if (byteIndex >= bytes.size) {
        throw RuntimeException("bit index is too large: $i")
    }

    val bitIndex = i % 8
    val shift = if ((bitOrder == BitOrder.BIG_ENDIAN)) bitIndex else (7 - bitIndex)
    val bitMask = 1 shl shift
    val currentByte = bytes[byteIndex]
    bytes[byteIndex] = (currentByte.toInt() or bitMask).toByte()
}

/**
 * Gets i-th bit in a bitmask.
 *
 * @param bytes    Bitmask.
 * @param bitOrder Order of bits in a byte
 * @param i        Bit index (0-based)
 * @return 1 if bit is set, 0 otherwise
 */
fun getBit(bytes: ByteArray, bitOrder: BitOrder, i: Int): Int {
    val byteIndex = (i / 8.0).toInt()
    if (byteIndex >= bytes.size) {
        throw RuntimeException("bit index is too large: $i")
    }

    val bitIndex = i % 8
    val shift = if ((bitOrder == BitOrder.BIG_ENDIAN)) bitIndex else (7 - bitIndex)
    val bitMask = 1 shl shift
    return (bytes[byteIndex].toInt() and bitMask) shr shift
}

/**
 * Check if i-th bit in the bitmask is set.
 *
 * @param bytes    Bitmask.
 * @param bitOrder Order of bits in a byte
 * @param i        Bit index (0-based)
 * @return true if i-th bit in the bitmask is set, false otherwise
 */
fun isSet(bytes: ByteArray, bitOrder: BitOrder, i: Int): Boolean {
    return getBit(bytes, bitOrder, i) == 1
}

/**
 * Get binary data from its' hex-encoded representation (regardless of case).
 *
 * @param s Hex-encoded representation of binary data
 * @return Binary data
 */
fun fromHex(s: String): ByteArray {
    require(!(s.isEmpty() || s.length % 2 != 0)) { "Invalid string: $s" }
    val chars = s.toCharArray()
    val len = chars.size / 2
    val bytes = ByteArray(len)
    var i = 0
    var j = 0
    while (i < len) {
        bytes[i] = (hexDigit(chars[j]) * 16 + hexDigit(
            chars[j + 1]
        )).toByte()
        i++
        j = i * 2
    }
    return bytes
}

/**
 * Get 20-bytes long info hash from its' base32-encoded representation (regardless of case).
 *
 * @param s base32-encoded representation of info hash
 * @return Binary data
 * @throws IllegalArgumentException if `s.length()` is not 32 characters long
 */
fun infoHashFromBase32(s: String): ByteArray {
    require(s.length == 32) { "Invalid string: $s" }
    val base32CodeBase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
    val hexCache = StringBuilder()
    var i = 0
    while (i < s.length) {
        val hexValue = base32CodeBase.indexOf(s[i]) shl 15 or (
                base32CodeBase.indexOf(s[i + 1]) shl 10) or (
                base32CodeBase.indexOf(s[i + 2]) shl 5) or
                base32CodeBase.indexOf(s[i + 3])
        hexCache.append(String.format("%05X", hexValue))
        i += 4
    }
    return fromHex(hexCache.toString())
}

private fun hexDigit(c: Char): Int {
    return when (c) {
        in '0'..'9' -> {
            c.code - '0'.code
        }

        in 'A'..'F' -> {
            c.code - 'A'.code + 10
        }

        in 'a'..'f' -> {
            c.code - 'a'.code + 10
        }

        else -> throw IllegalArgumentException("Illegal hexadecimal character: $c")
    }
}

/**
 * Returns a copy of the provided byte array with each byte reversed.
 *
 * @return A copy of the provided byte array with each byte reversed.

 */
fun reverseBits(bytes: ByteArray): ByteArray {
    val arr = bytes.copyOf(bytes.size)
    for (i in arr.indices) {
        arr[i] = reverseBits(arr[i])
    }
    return arr
}

/**
 * Returns a reversed byte.
 *
 * @return A reversed byte.
 */
private fun reverseBits(b: Byte): Byte {
    var i = b.toInt()
    // swap halves
    i = (i and 240) shr 4 or ((i and 15) shl 4)
    // swap adjacent pairs
    i = (i and 204) shr 2 or ((i and 51) shl 2)
    // swap adjacent bits
    i = (i and 170) shr 1 or ((i and 85) shl 1)
    return i.toByte()
}

