package tech.lp2p.loki.protocol

enum class BitOrder {
    BIG_ENDIAN,
    LITTLE_ENDIAN
}
