package tech.lp2p.loki.protocol

import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.net.Peer
import java.nio.ByteBuffer

class UnchokeHandler : UniqueMessageHandler(Type.Unchoke) {
    public override fun doDecode(peer: Peer, buffer: ByteBufferView): DecodingResult {
        verifyPayloadHasLength(Type.Unchoke, 0, buffer.remaining())
        return DecodingResult(0, unchoke())
    }

    public override fun doEncode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean {
        return true
    }
}
