package tech.lp2p.loki.protocol

class Interested : Message {
    override val messageId: Byte
        get() = StandardBittorrentProtocol.INTERESTED_ID
    override val type: Type
        get() = Type.Interested
}

private val instance = Interested()

fun interested(): Interested {
    return instance
}