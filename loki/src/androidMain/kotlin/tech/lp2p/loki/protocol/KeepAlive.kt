package tech.lp2p.loki.protocol

class KeepAlive : Message {
    override val messageId: Byte
        get() {
            throw UnsupportedOperationException()
        }
    override val type: Type
        get() = Type.KeepAlive
}

private val instance = KeepAlive()

fun keepAlive(): KeepAlive {
    return instance
}