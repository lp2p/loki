package tech.lp2p.loki.protocol


data class Port(val port: Int) : Message {

    override fun toString(): String {
        return "[Port] port {$port}"
    }

    override val type: Type
        get() = Type.Port

    override val messageId: Byte
        get() = StandardBittorrentProtocol.PORT_ID

    init {
        require(!(port < 0 || port > 65535)) { "Invalid argument: port ($port)" }
    }
}
