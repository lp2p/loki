package tech.lp2p.loki.net

import tech.lp2p.loki.Loki
import tech.lp2p.loki.protocol.ExtendedHandshake
import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.Type
import tech.lp2p.loki.torrent.Consumers
import tech.lp2p.loki.torrent.MessageConsumer
import tech.lp2p.loki.torrent.MessageContext


internal data class ExtendedHandshakeConsumer(private val loki: Loki) : Consumers {
    private fun doConsume(message: Message, messageContext: MessageContext) {
        if (message is ExtendedHandshake) {
            consume(message, messageContext)
        }
    }

    override val consumers: List<MessageConsumer>
        get() {
            val list: MutableList<MessageConsumer> = ArrayList()
            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.ExtendedHandshake
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })
            return list
        }

    private fun consume(message: ExtendedHandshake, messageContext: MessageContext) {
        val peerPort = message.port
        if (peerPort != null) {
            loki.connectionPool.checkDuplicateConnections(messageContext.peer)
        }
    }
}
