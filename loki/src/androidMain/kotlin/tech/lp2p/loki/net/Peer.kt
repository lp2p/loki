package tech.lp2p.loki.net

import tech.lp2p.loki.protocol.EncryptionPolicy
import java.net.InetAddress

data class Peer(
    val inetAddress: InetAddress,
    val port: Int,
    val encryptionPolicy: EncryptionPolicy
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Peer

        if (port != other.port) return false
        if (inetAddress != other.inetAddress) return false

        return true
    }

    override fun hashCode(): Int {
        var result = port
        result = 31 * result + inetAddress.hashCode()
        return result
    }
}

private fun checkPort(port: Int) {
    require(!(port < 0 || port > 65535)) { "Invalid port: $port" }
}

fun createPeer(holder: InetPeerAddress): Peer {
    val port = holder.port
    checkPort(port)
    return Peer(InetAddress.getByName(holder.hostname), port, EncryptionPolicy.PREFER_PLAINTEXT)
}

fun createPeer(address: InetAddress, port: Int): Peer {
    checkPort(port)
    return Peer(address, port, EncryptionPolicy.PREFER_PLAINTEXT)
}

fun createPeer(address: InetAddress, port: Int, encryptionPolicy: EncryptionPolicy): Peer {
    checkPort(port)
    return Peer(address, port, encryptionPolicy)
}