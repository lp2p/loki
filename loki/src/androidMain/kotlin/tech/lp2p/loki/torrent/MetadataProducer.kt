package tech.lp2p.loki.torrent

import tech.lp2p.loki.META_EXCHANGE_BLOCK_SIZE
import tech.lp2p.loki.debug
import tech.lp2p.loki.net.Peer
import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.MetaType
import tech.lp2p.loki.protocol.Type
import tech.lp2p.loki.protocol.UtMetadata
import tech.lp2p.loki.protocol.data
import tech.lp2p.loki.protocol.reject
import java.nio.ByteBuffer
import java.util.Queue
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap
import java.util.concurrent.LinkedBlockingQueue
import kotlin.math.min


class MetadataProducer(
    private val source: ByteBuffer,
    private val isPrivate: Boolean
) : Produces, Consumers {
    private val outboundMessages: ConcurrentMap<Peer, Queue<Message>> = ConcurrentHashMap()

    private fun doConsume(message: Message, messageContext: MessageContext) {
        if (message is UtMetadata) {
            consume(message, messageContext)
        }
    }

    override val consumers: List<MessageConsumer>
        get() {
            val list: MutableList<MessageConsumer> =
                ArrayList()
            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.UtMetadata
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })
            return list
        }

    private fun consume(message: UtMetadata, context: MessageContext) {
        val peer = context.peer
        // being lenient herer and not checking if the peer advertised ut_metadata support
        if (message.metaType == MetaType.REQUEST) { // TODO: spam protection
            processMetadataRequest(peer, message.pieceIndex)
        }
    }

    private fun processMetadataRequest(peer: Peer, pieceIndex: Int) {
        val response: Message

        if (isPrivate) {
            // reject all requests if:
            // - torrent is private
            response = reject(pieceIndex)
        } else {

            val size = source.capacity()
            val offset = pieceIndex * META_EXCHANGE_BLOCK_SIZE.toLong()
            if (offset > size) {
                response = reject(pieceIndex) // not valid piece index
            } else {
                val length = min(size - offset, META_EXCHANGE_BLOCK_SIZE.toLong())


                debug("MetadataProducer", "Offset $offset Length $length")

                val data = ByteArray(length.toInt())
                source.get(data, offset.toInt(), data.size)

                response = data(pieceIndex, size.toInt(), data) // todo optimize
            }
        }

        getOrCreateOutboundMessages(peer).add(response)
    }


    private fun getOrCreateOutboundMessages(peer: Peer): Queue<Message> {
        var queue = outboundMessages[peer]
        if (queue == null) {
            queue = LinkedBlockingQueue()
            val existing = outboundMessages.putIfAbsent(peer, queue)
            if (existing != null) {
                queue = existing
            }
        }
        return queue
    }


    override fun produce(context: MessageContext, messageConsumer: (Message) -> Unit) {
        val peer = context.peer

        val queue = outboundMessages[peer]

        if (!queue.isNullOrEmpty()) {
            val msg = queue.poll()
            if (msg != null) {
                messageConsumer.invoke(msg)
            }
        }
    }
}
