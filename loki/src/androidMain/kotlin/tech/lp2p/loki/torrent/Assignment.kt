package tech.lp2p.loki.torrent

import tech.lp2p.loki.MAX_PIECE_RECEIVING_TIME
import tech.lp2p.loki.MAX_SIMULTANEOUSLY_ASSIGNED_PIECES
import tech.lp2p.loki.net.Peer

class Assignment internal constructor(
    val peer: Peer,
    private val selector: ValidatingSelector,
    private val pieceStatistics: PieceStatistics,
    private val assignments: Assignments
) {

    val pieces: ArrayDeque<Int> = ArrayDeque()
    private var connectionState: ConnectionState? = null
    private var started: Long = 0
    private var checked: Long = 0
    private var aborted = false

    init {
        claimPiecesIfNeeded()
    }

    private fun claimPiecesIfNeeded() {
        if (pieces.size < MAX_SIMULTANEOUSLY_ASSIGNED_PIECES) {
            val peerBitfield = pieceStatistics.getPeerBitfield(peer)
            checkNotNull(peerBitfield)

            // randomize order of pieces to keep the number of pieces
            // requested from different peers at the same time to a minimum
            val requiredPieces = selector.getNextPieces(pieceStatistics).toArray()
            if (assignments.isEndgame) {
                requiredPieces.shuffle()
            }

            var i = 0
            while (i < requiredPieces.size && pieces.size < 3) {
                val pieceIndex = requiredPieces[i]
                if (peerBitfield.isVerified(pieceIndex) && assignments.claim(pieceIndex)) {
                    pieces.add(pieceIndex)
                }
                i++
            }
        }
    }

    fun isAssigned(pieceIndex: Int): Boolean {
        return pieces.contains(pieceIndex)
    }

    val status: Status
        get() {
            if (started > 0) {
                val duration = System.currentTimeMillis() - checked
                if (duration > MAX_PIECE_RECEIVING_TIME) {
                    return Status.TIMEOUT
                }
            }
            return Status.ACTIVE
        }

    fun start(connectionState: ConnectionState) {
        check(this.connectionState == null) { "Assignment is already started" }
        check(!aborted) { "Assignment is aborted" }
        this.connectionState = connectionState
        connectionState.currentAssignment = this
        started = System.currentTimeMillis()
        checked = started
    }

    fun check() {
        checked = System.currentTimeMillis()
    }

    fun finish(pieceIndex: Int) {
        if (pieces.remove(pieceIndex)) {
            assignments.finish(pieceIndex)
            claimPiecesIfNeeded()
        }
    }

    fun abort() {
        aborted = true
        if (connectionState != null) {
            connectionState!!.removeAssignment()
        }
    }

    enum class Status {
        ACTIVE, TIMEOUT
    }
}

