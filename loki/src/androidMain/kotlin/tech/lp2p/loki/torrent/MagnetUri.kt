package tech.lp2p.loki.torrent

import tech.lp2p.loki.net.InetPeerAddress

@Suppress("unused")
class MagnetUri private constructor(
    /**
     * Represents the "xt" parameter.
     * E.g. xt=urn:btih:af0d9aa01a9ae123a73802cfa58ccaf355eb19f1
     */
    val torrentId: TorrentId,
    /**
     * Represents the "dn" parameter. Value is URL decoded.
     * E.g. dn=Some%20Display%20Name =&gt; "Some Display Name"
     */
    val displayName: String?,
    val trackerUrls: Collection<String>,
    /**
     * Represents the collection of values of "x.pe" parameters. Values are URL decoded.
     * E.g. `x.pe=124.131.72.242%3A6891&x.pe=11.9.132.61%3A6900`
     * =&gt; [124.131.72.242:6891, 11.9.132.61:6900]
     *
     * @return Collection of well-known peer addresses
     */
    val peerAddresses: Collection<InetPeerAddress>
) {


    class Builder internal constructor(private val torrentId: TorrentId) {
        private val trackerUrls: MutableSet<String> = HashSet()
        private val peerAddresses: MutableSet<InetPeerAddress> = HashSet()
        private var displayName: String? = null

        /**
         * Set "dn" parameter.
         * Caller must NOT perform URL encoding, otherwise the value will get encoded twice.
         */
        fun name(displayName: String) {
            this.displayName = displayName
        }

        /**
         * Add "tr" parameter.
         * Caller must NOT perform URL encoding, otherwise the value will get encoded twice.
         */
        fun tracker(trackerUrl: String) {
            trackerUrls.add(trackerUrl)
        }

        /**
         * Add "x.pe" parameter.
         *
         * @param peerAddress Well-known peer address
         */
        fun peer(peerAddress: InetPeerAddress) {
            peerAddresses.add(peerAddress)
        }

        fun buildUri(): MagnetUri {
            return MagnetUri(torrentId, displayName, trackerUrls, peerAddresses)
        }
    }

    companion object {
        fun torrentId(torrentId: TorrentId): Builder {
            return Builder(torrentId)
        }
    }
}
