package tech.lp2p.loki.torrent

import kotlinx.atomicfu.atomic
import tech.lp2p.loki.Loki
import tech.lp2p.loki.META_EXCHANGE_BLOCK_SIZE
import tech.lp2p.loki.META_EXCHANGE_MAX_SIZE
import tech.lp2p.loki.UT_METADATA
import tech.lp2p.loki.net.Peer
import tech.lp2p.loki.protocol.ExtendedHandshake
import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.MetaType
import tech.lp2p.loki.protocol.Type
import tech.lp2p.loki.protocol.UtMetadata
import tech.lp2p.loki.protocol.request
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap

class MetadataConsumer internal constructor(
    private val loki: Loki,
    private val torrentId: TorrentId
) : Produces, Consumers {

    private val peersWithoutMetadata: ConcurrentMap<Peer, Long> = ConcurrentHashMap()

    private val supportingPeers: MutableSet<Peer> = ConcurrentHashMap.newKeySet()
    private val requestedFirstPeers: ConcurrentMap<Peer, Long> =
        ConcurrentHashMap()
    private val requestedAllPeers: MutableSet<Peer> = ConcurrentHashMap.newKeySet()


    // set immediately after metadata has been fetched and verified
    private val done = atomic(false)

    @Volatile
    private var metadata: ExchangedMetadata? = null

    @Volatile
    private var doConsume: Boolean = true

    private fun doConsume(message: Message, messageContext: MessageContext) {
        if (message is ExtendedHandshake) {
            consume(message, messageContext)
        }
        if (message is UtMetadata) {
            consume(message, messageContext)
        }
    }

    override val consumers: List<MessageConsumer>
        get() {
            val list: MutableList<MessageConsumer> =
                ArrayList()
            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.ExtendedHandshake
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })
            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.UtMetadata
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })
            return list
        }

    private fun consume(handshake: ExtendedHandshake, messageContext: MessageContext) {
        if (handshake.supportedMessageTypes.contains(UT_METADATA)) {
            // moreover the extended handshake message type map is additive,
            // so we can't learn about the peer turning off extensions solely from the message
            supportingPeers.add(messageContext.peer)
        }
    }


    private fun consume(message: UtMetadata, context: MessageContext) {
        if (doConsume) {
            val peer = context.peer
            // being lenient herer and not checking if the peer advertised ut_metadata support
            when (message.metaType) {
                MetaType.DATA -> {
                    val totalSize = message.totalSize
                    check(totalSize < META_EXCHANGE_MAX_SIZE) {
                        "Declared metadata size is too large: " + totalSize +
                                "; max allowed is " + META_EXCHANGE_MAX_SIZE
                    }
                    processMetadataBlock(message.pieceIndex, totalSize, message.data)

                    peersWithoutMetadata.put(peer, System.currentTimeMillis())

                }

                MetaType.REJECT -> {
                    peersWithoutMetadata.put(peer, System.currentTimeMillis())
                }

                else -> {}
            }
        }
    }

    private fun processMetadataBlock(pieceIndex: Int, totalSize: Int, data: ByteArray) {
        if (metadata == null) {
            metadata = ExchangedMetadata(totalSize, META_EXCHANGE_BLOCK_SIZE)
        }

        if (!metadata!!.isBlockPresent(pieceIndex)) {
            metadata!!.setBlock(pieceIndex, data)

            if (metadata!!.isComplete) {
                val digest = metadata!!.digest()
                if (digest.contentEquals(torrentId.bytes)) {
                    var fetchedTorrent: Torrent? = null
                    try {
                        fetchedTorrent = buildTorrent(metadata!!.data.array())
                    } catch (_: Exception) {
                        metadata = null
                    }

                    if (fetchedTorrent != null) {
                        loki.torrent = fetchedTorrent
                        loki.source = metadata!!.data.asReadOnlyBuffer()
                        synchronized(done) {
                            done.value = true
                            requestedFirstPeers.clear()
                            requestedAllPeers.clear()
                            (done as Object).notifyAll()
                        }
                        doConsume = false
                        metadata = null
                    }
                } else {
                    // restart the process
                    // TODO: terminate peer connections that the metadata was fetched from?
                    // or just try again with the others?

                    metadata = null
                }
            }
        }
    }


    override fun produce(context: MessageContext, messageConsumer: (Message) -> Unit) {
        // stop here if metadata has already been fetched
        if (done.value) {
            return
        }

        val peer = context.peer
        if (supportingPeers.contains(peer)) {
            if (peersWithoutMetadata.containsKey(peer)) {
                if ((System.currentTimeMillis() -
                            checkNotNull(peersWithoutMetadata[peer]))
                    >= WAIT_BEFORE_REREQUESTING_AFTER_REJECT
                ) {
                    peersWithoutMetadata.remove(peer)
                }
            }

            if (!peersWithoutMetadata.containsKey(peer)) {
                if (metadata == null) {
                    if (!requestedFirstPeers.containsKey(peer) ||
                        (System.currentTimeMillis() -
                                checkNotNull(requestedFirstPeers[peer])
                                > FIRST_BLOCK_ARRIVAL_TIMEOUT)
                    ) {
                        requestedFirstPeers[peer] = System.currentTimeMillis()
                        // start with the first piece of metadata
                        messageConsumer.invoke(request(0))
                    }
                } else if (!requestedAllPeers.contains(peer)) {
                    requestedAllPeers.add(peer)
                    // TODO: larger metadata should be handled in more intelligent way
                    // starting with block #1 because by now we should have already received block #0
                    for (i in 1 until metadata!!.blockCount) {
                        messageConsumer.invoke(request(i))
                    }
                }
            }
        }
    }

    /**
     * @return Torrent, blocking the calling thread if it hasn't been fetched yet
     */
    fun waitForTorrent() {
        while (!done.value) {
            synchronized(done) {
                if (!done.value) {
                    (done as Object).wait()
                }
            }
        }
    }
}


private const val FIRST_BLOCK_ARRIVAL_TIMEOUT: Long = 10000 // 10 sec
private const val WAIT_BEFORE_REREQUESTING_AFTER_REJECT: Long = 10000 // 10 sec
