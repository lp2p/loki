package tech.lp2p.loki.torrent

import tech.lp2p.loki.data.Bitfield
import tech.lp2p.loki.net.Peer

class Assignments(
    private val bitfield: Bitfield,
    private val selector: ValidatingSelector,
    private val pieceStatistics: PieceStatistics
) {
    private val assignedPieces: MutableSet<Int> = HashSet()
    private val assignments: MutableMap<Peer, Assignment> = HashMap()

    fun get(peer: Peer): Assignment? {
        return assignments[peer]
    }

    fun remove(assignment: Assignment) {
        assignment.abort()
        assignments.remove(assignment.peer)
        assignedPieces.removeAll(assignment.pieces)
    }

    fun count(): Int {
        return assignments.size
    }

    fun assign(peer: Peer): Assignment? {
        if (!hasInterestingPieces(peer)) {
            return null
        }

        val assignment = Assignment(
            peer,
            selector, pieceStatistics, this
        )
        assignments[peer] = assignment
        return assignment
    }

    fun claim(pieceIndex: Int): Boolean {
        val claimed = !bitfield.isComplete(pieceIndex) && (isEndgame
                || !assignedPieces.contains(pieceIndex))
        if (claimed) {
            assignedPieces.add(pieceIndex)
        }
        return claimed
    }

    fun finish(pieceIndex: Int) {
        assignedPieces.remove(pieceIndex)
    }

    val isEndgame: Boolean
        get() =// if all remaining pieces are requested,
            // that would mean that we have entered the "endgame" mode
            bitfield.piecesRemaining <= assignedPieces.size

    /**
     * @return Collection of peers that have interesting pieces and can be given an assignment
     */
    fun update(ready: Set<Peer>, choking: Set<Peer>): Set<Peer> {
        val result: MutableSet<Peer> = HashSet()
        for (peer in ready) {
            if (hasInterestingPieces(peer)) {
                result.add(peer)
            }
        }
        for (peer in choking) {
            if (hasInterestingPieces(peer)) {
                result.add(peer)
            }
        }

        return result
    }

    private fun hasInterestingPieces(peer: Peer): Boolean {
        val peerBitfield = pieceStatistics.getPeerBitfield(peer) ?: return false
        val peerBitmask = peerBitfield.getBitmask()
        val localBitfield = bitfield.getBitmask()
        peerBitmask.andNot(localBitfield)
        return peerBitmask.cardinality() > 0
    }
}
