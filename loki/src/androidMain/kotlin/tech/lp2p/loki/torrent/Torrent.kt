package tech.lp2p.loki.torrent

import tech.lp2p.loki.UNDEFINED_TORRENT_NAME
import tech.lp2p.loki.bencoding.BEInteger
import tech.lp2p.loki.bencoding.BEList
import tech.lp2p.loki.bencoding.BEMap
import tech.lp2p.loki.bencoding.BEObject
import tech.lp2p.loki.bencoding.BEString
import tech.lp2p.loki.bencoding.BEType
import tech.lp2p.loki.bencoding.createParser
import tech.lp2p.loki.debug
import tech.lp2p.loki.protocol.Message
import java.math.BigInteger
import kotlin.math.min


interface Agent

interface Consumers : Agent {
    val consumers: List<MessageConsumer>
}

interface Produces : Agent {
    fun produce(context: MessageContext, messageConsumer: (Message) -> Unit)
}

data class TorrentId(val bytes: ByteArray) {
    override fun hashCode(): Int {
        return bytes.contentHashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TorrentId

        return bytes.contentEquals(other.bytes)
    }
}


data class TorrentFile(
    val size: Long,
    val pieces: MutableSet<Int>,
    val pathElements: List<String>
) {

    fun addPiece(piece: Int) {
        pieces.add(piece)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }

        val that = other as TorrentFile
        return size == that.size && pathElements == that.pathElements
    }

    override fun hashCode(): Int {
        var result = size.hashCode()
        result = 31 * result + pathElements.hashCode()
        return result
    }
}

fun createTorrentFile(size: Long, pathElements: List<String>): TorrentFile {
    if (size < 0) {
        throw RuntimeException("Invalid torrent file size: $size")
    }
    if (pathElements.isEmpty()) {
        throw RuntimeException("Can't create threads.torrent file without path")
    }

    return TorrentFile(size, HashSet(), pathElements)
}


private const val CHUNK_HASH_LENGTH = 20
const val TORRENT_ID_LENGTH = 20

fun createTorrentId(torrentId: ByteArray): TorrentId {
    if (torrentId.size != TORRENT_ID_LENGTH) {
        throw RuntimeException("Illegal threads.torrent ID length: " + torrentId.size)
    }
    return TorrentId(torrentId)
}


data class Torrent(
    val name: String,
    val files: List<TorrentFile>,
    val chunkHashes: List<ByteArray>,
    val size: Long,
    val chunkSize: Long,
    val isPrivate: Boolean,
    val creationDate: Long,
    val createdBy: String,
    val singleFile: Boolean
) {
    override fun toString(): String {
        return "Torrent{" +
                ", name='" + name + '\'' +
                ", files=" + files +
                ", chunkSize=" + chunkSize +
                ", chunkHashes=" + chunkHashes.size +
                ", size=" + size +
                ", creationDate=" + creationDate +
                ", isPrivate=" + isPrivate +
                ", createdBy='" + createdBy + '\'' +
                '}'
    }

    fun build() {
        val ranges: MutableMap<Long, TorrentFile> = HashMap()
        var startRange = 0L
        for (file in files) {
            debug("Torrent", "start $startRange $file")
            ranges[startRange] = file
            startRange += file.size
        }

        val chunkSize = this.chunkSize


        var chunks = 0
        var off: Long
        var lim: Long
        var remaining = size
        while (remaining > 0) {
            off = chunks * chunkSize
            lim = min(chunkSize, remaining)

            val chunkFiles: MutableList<TorrentFile> = mutableListOf()
            for ((start, entry) in ranges) {
                if (start <= (lim + off)) {
                    if (off <= start + entry.size) {
                        chunkFiles.add(entry)
                    }
                }
            }
            for (tf in chunkFiles) {
                tf.addPiece(chunks)
            }

            chunks++

            remaining -= chunkSize
        }
    }

}

fun buildHashes(hashes: ByteArray): List<ByteArray> {
    val result: MutableList<ByteArray> = mutableListOf()
    var read = 0
    while (read < hashes.size) {
        val start = read
        read += CHUNK_HASH_LENGTH
        result.add(hashes.copyOfRange(start, read))
    }
    return result
}

fun buildTorrent(bs: ByteArray): Torrent {

    val parser = createParser(bs)
    if (parser.readType() != BEType.MAP) {
        throw RuntimeException(
            "Invalid metainfo format -- expected a map, got: "
                    + parser.readType().name.lowercase()
        )
    }
    val metadata = parser.readMap()
    val infoDictionary: BEMap?
    val root = metadata.value
    infoDictionary = if (root.containsKey(INFOMAP_KEY)) {
        // standard BEP-3 format
        root[INFOMAP_KEY] as BEMap?
    } else {
        // BEP-9 exchanged metadata (just the info dictionary)
        metadata
    }
    checkNotNull(infoDictionary)

    val infoMap = infoDictionary.value

    var name = UNDEFINED_TORRENT_NAME
    if (infoMap[TORRENT_NAME_KEY] != null) {
        val data = (checkNotNull(
            infoMap[TORRENT_NAME_KEY]
        ) as BEString).content
        name = String(data, Charsets.UTF_8)
    }

    val chunkSize = (checkNotNull(
        infoMap[CHUNK_SIZE_KEY]
    ) as BEInteger).value


    val chunkHashes =
        (checkNotNull(infoMap[CHUNK_HASHES_KEY]) as BEString).content


    val torrentFiles: MutableList<TorrentFile> = ArrayList()
    val size: Long
    if (infoMap[TORRENT_SIZE_KEY] != null) {
        val torrentSize = (checkNotNull(
            infoMap[TORRENT_SIZE_KEY]
        ) as BEInteger).value
        size = torrentSize.toLong()
    } else {
        val files =
            (checkNotNull(infoMap[FILES_KEY]) as BEList).value
        var torrentSize = BigInteger.ZERO
        for (data in files) {
            val file = data as BEMap
            val fileMap = file.value

            val fileSize = (checkNotNull(
                fileMap[FILE_SIZE_KEY]
            ) as BEInteger).value

            torrentSize = torrentSize.add(fileSize)

            val objectList = (checkNotNull(fileMap[FILE_PATH_ELEMENTS_KEY]) as BEList).value

            val pathElements: MutableList<BEString> = ArrayList()
            objectList.forEach { beObject: BEObject ->
                pathElements.add(
                    beObject as BEString
                )
            }

            torrentFiles.add(
                createTorrentFile(
                    fileSize.toLong(), pathElements
                        .map { obj: BEString -> obj.string() })
            )
        }

        size = torrentSize.toLong()
    }
    var isPrivate = false
    if (infoMap[PRIVATE_KEY] != null) {
        if (BigInteger.ONE == (checkNotNull(
                infoMap[PRIVATE_KEY]
            ) as BEInteger).value
        ) {
            isPrivate = true
        }
    }
    var creationDate = System.currentTimeMillis()
    if (root[CREATION_DATE_KEY] != null) {
        // some torrents contain bogus values here (like 101010101010), which causes an exception
        try {
            val epochMilli = (checkNotNull(
                root[CREATION_DATE_KEY]
            ) as BEInteger).value
            creationDate = epochMilli.toInt().toLong()
        } catch (throwable: Throwable) {
            debug("Torrent", throwable)
        }
    }
    var createdBy = ""

    if (root[CREATED_BY_KEY] != null) {
        createdBy = (checkNotNull(
            root[CREATED_BY_KEY]
        ) as BEString).string()
    }
    return createTorrent(
        name, torrentFiles,
        chunkHashes, size, chunkSize.toLong(),
        isPrivate, creationDate, createdBy
    )
}

const val INFOMAP_KEY: String = "info"
const val TORRENT_NAME_KEY: String = "name"
const val CHUNK_SIZE_KEY: String = "piece length"
const val CHUNK_HASHES_KEY: String = "pieces"
const val TORRENT_SIZE_KEY: String = "length"
const val FILES_KEY: String = "files"
const val FILE_SIZE_KEY: String = "length"
const val FILE_PATH_ELEMENTS_KEY: String = "path"
const val PRIVATE_KEY: String = "private"
const val CREATION_DATE_KEY: String = "creation date"
const val CREATED_BY_KEY: String = "created by"


fun createTorrent(
    name: String,
    torrentFiles: MutableList<TorrentFile>, chunkHashes: ByteArray,
    size: Long, chunkSize: Long, isPrivate: Boolean,
    creationDate: Long, createdBy: String
): Torrent {
    require(chunkHashes.size.mod(CHUNK_HASH_LENGTH) == 0) {
        "Invalid chunk hashes string -- length (" + chunkHashes.size +
                ") is not divisible by " + CHUNK_HASH_LENGTH
    }
    var singleFile = false
    if (torrentFiles.isEmpty()) {
        torrentFiles.add(createTorrentFile(size, listOf(name)))
        singleFile = true
    }

    val hashes: List<ByteArray> = buildHashes(chunkHashes)

    val torrent = Torrent(
        name, torrentFiles, hashes,
        size, chunkSize, isPrivate, creationDate, createdBy, singleFile
    )
    torrent.build()
    debug("Torrent", torrent.toString())
    return torrent
}

