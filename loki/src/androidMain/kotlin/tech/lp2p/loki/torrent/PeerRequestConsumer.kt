package tech.lp2p.loki.torrent

import tech.lp2p.loki.debug
import tech.lp2p.loki.net.Peer
import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.Request
import tech.lp2p.loki.protocol.Type
import tech.lp2p.loki.protocol.createPiece
import java.util.Queue
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedQueue

internal class PeerRequestConsumer(private val dataWorker: DataWorker) :
    Produces, Consumers {
    private val completedRequests: MutableMap<Peer, Queue<BlockRead>> =
        ConcurrentHashMap()

    private fun doConsume(message: Message, messageContext: MessageContext) {
        if (message is Request) {
            consume(message, messageContext)
        }
    }

    override val consumers: List<MessageConsumer>
        get() {
            val list: MutableList<MessageConsumer> =
                ArrayList()
            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.Request
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })

            return list
        }


    private fun consume(request: Request, context: MessageContext) {
        val connectionState = context.connectionState
        if (!connectionState.isChoking()) {
            try {
                val block: BlockRead = dataWorker.addBlockRequest(
                    request.pieceIndex,
                    request.offset,
                    request.length
                )
                if (block.isRejected) {
                    debug("PeerRequestConsumer", "Block rejected")
                } else {
                    getCompletedRequestsForPeer(context.peer).add(block)
                }
            } catch (throwable: Throwable) {
                debug("PeerRequestConsumer", throwable)
            }
        }
    }


    private fun getCompletedRequestsForPeer(peer: Peer): Queue<BlockRead> {
        var queue = completedRequests[peer]
        if (queue == null) {
            queue = ConcurrentLinkedQueue()
            val existing = completedRequests.putIfAbsent(peer, queue)
            if (existing != null) {
                queue = existing
            }
        }
        return queue
    }


    override fun produce(context: MessageContext, messageConsumer: (Message) -> Unit) {
        val peer = context.peer
        val queue = getCompletedRequestsForPeer(peer)
        var block: BlockRead?
        while ((queue.poll().also { block = it }) != null) {
            checkNotNull(block)
            messageConsumer.invoke(
                createPiece(
                    block.pieceIndex, block.offset,
                    block.length, block.blockRange!!
                )
            )
        }
    }
}
