package tech.lp2p.loki.buffer

import java.nio.ByteBuffer


class BufferedData(val buffer: ByteBuffer, val length: Int) {
    @Volatile
    var isDisposed: Boolean = false
        private set

    fun dispose() {
        isDisposed = true
    }
}
