package tech.lp2p.loki.buffer

import java.nio.ByteBuffer


data class DelegatingByteBufferView(val delegate: ByteBuffer) : ByteBufferView {

    override fun position(): Int {
        return delegate.position()
    }

    override fun position(newPosition: Int) {
        delegate.position(newPosition)
    }

    override fun limit(): Int {
        return delegate.limit()
    }

    override fun limit(newLimit: Int) {
        delegate.limit(newLimit)
    }

    override fun hasRemaining(): Boolean {
        return delegate.hasRemaining()
    }

    override fun remaining(): Int {
        return delegate.remaining()
    }

    override fun get(): Byte {
        return delegate.get()
    }

    override val short: Short
        get() = delegate.getShort()

    override val int: Int
        get() = delegate.getInt()

    override fun get(dst: ByteArray) {
        delegate[dst]
    }

    override fun duplicate(): ByteBufferView {
        return DelegatingByteBufferView(delegate.duplicate())
    }
}
