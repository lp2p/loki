package tech.lp2p.loki.buffer

import tech.lp2p.loki.protocol.readInt
import tech.lp2p.loki.protocol.readShort
import java.nio.ByteBuffer
import java.nio.ByteOrder

class SplicedByteBufferView private constructor(
    private val left: ByteBuffer,
    private val right: ByteBuffer,
    private var position: Int,
    private var limit: Int,
    private val capacity: Int,
    private val leftOffset: Int,
    private val leftLimit: Int,
    private val rightOffset: Int,
    private val rightLimit: Int
) :
    ByteBufferView {
    private val leftCapacity = leftLimit - leftOffset
    private val shortBytes = ByteArray(Short.SIZE_BYTES)
    private val intBytes = ByteArray(Int.SIZE_BYTES)

    constructor(left: ByteBuffer, right: ByteBuffer) : this(
        left, right, 0,
        left.remaining() + right.remaining(),
        left.remaining() + right.remaining(),
        left.position(), left.limit(),
        right.position(), right.limit()
    ) {
        // mandate that the order is BE, otherwise it will be impossible to read multi-byte numbers,
        // that sit on the boundary of two buffers
        require(!(left.order() != ByteOrder.BIG_ENDIAN || right.order() != ByteOrder.BIG_ENDIAN)) { "Byte order must be big-endian for both buffers" }
    }

    override fun position(): Int {
        return position
    }

    override fun position(newPosition: Int) {
        require(newPosition <= limit) { "Position is greater than limit: $newPosition > $limit" }
        require(newPosition >= 0) { "Negative position: $newPosition" }
        position = newPosition
        if (position >= leftCapacity) {
            left.position(leftLimit)
            right.position(rightOffset + (position - leftCapacity))
        } else {
            left.position(leftOffset + position)
            right.position(rightOffset)
        }
    }

    override fun limit(): Int {
        return limit
    }

    override fun limit(newLimit: Int) {
        require(newLimit <= capacity) { "Limit is greater than capacity: $newLimit > $capacity" }
        require(newLimit >= 0) { "Negative limit: $newLimit" }
        limit = newLimit
        if (limit >= leftCapacity) {
            left.limit(leftLimit)
            right.limit(limit - leftCapacity)
        } else {
            left.limit(leftOffset + limit)
            right.limit(rightOffset)
        }
        if (position > limit) {
            position = limit
        }
    }

    override fun hasRemaining(): Boolean {
        return position < limit
    }

    override fun remaining(): Int {
        return limit - position
    }

    override fun get(): Byte {
        require(position < limit) { "Insufficient space: $position >= $limit" }
        return if (position++ >= leftCapacity) {
            right.get()
        } else {
            left.get()
        }
    }

    override val short: Short
        get() {
            readBytes(shortBytes)
            return readShort(shortBytes, 0)
        }

    override val int: Int
        get() {
            readBytes(intBytes)
            return readInt(intBytes, 0)
        }

    override fun get(dst: ByteArray) {
        readBytes(dst)
    }

    private fun readBytes(dst: ByteArray) {
        require(limit - position >= dst.size) { "Insufficient space: " + (limit - position) + " < " + dst.size }
        if (left.remaining() >= dst.size) {
            left[dst]
        } else if (!left.hasRemaining()) {
            right[dst]
        } else {
            // combine from two buffers
            val bytesFromLeft = left.remaining()
            val bytesFromRight = dst.size - left.remaining()
            left[dst, 0, left.remaining()]
            right[dst, bytesFromLeft, bytesFromRight]
        }
        position += dst.size
    }

    override fun duplicate(): ByteBufferView {
        val leftDup = left.duplicate()
        val rightDup = right.duplicate()
        return SplicedByteBufferView(
            leftDup, rightDup,
            position, limit, capacity, leftOffset, leftLimit, rightOffset, rightLimit
        )
    }
}
