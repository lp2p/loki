package tech.lp2p.loki

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import tech.lp2p.idun.Idun
import tech.lp2p.idun.core.createRandomKey
import tech.lp2p.idun.newIdun
import tech.lp2p.idun.nextFreePort
import tech.lp2p.loki.data.Bitfield
import tech.lp2p.loki.data.Storage
import tech.lp2p.loki.net.BitfieldConnectionHandler
import tech.lp2p.loki.net.BufferedPieceRegistry
import tech.lp2p.loki.net.ConnectionSource
import tech.lp2p.loki.net.DHTHandshakeHandler
import tech.lp2p.loki.net.DataReceiver
import tech.lp2p.loki.net.ExtendedProtocolHandshakeHandler
import tech.lp2p.loki.net.InetPeerAddress
import tech.lp2p.loki.net.MessageDispatcher
import tech.lp2p.loki.net.Peer
import tech.lp2p.loki.net.PeerConnectionFactory
import tech.lp2p.loki.net.PeerConnectionPool
import tech.lp2p.loki.net.SocketChannelConnectionAcceptor
import tech.lp2p.loki.net.createPeer
import tech.lp2p.loki.protocol.EncryptionPolicy
import tech.lp2p.loki.protocol.ExtendedMessageHandler
import tech.lp2p.loki.protocol.ExtendedProtocol
import tech.lp2p.loki.protocol.MessageHandler
import tech.lp2p.loki.protocol.PeerExchangeMessageHandler
import tech.lp2p.loki.protocol.PortMessageHandler
import tech.lp2p.loki.protocol.StandardBittorrentProtocol
import tech.lp2p.loki.protocol.UtMetadataMessageHandler
import tech.lp2p.loki.protocol.fromHex
import tech.lp2p.loki.protocol.infoHashFromBase32
import tech.lp2p.loki.torrent.Assignments
import tech.lp2p.loki.torrent.BitfieldCollectingConsumer
import tech.lp2p.loki.torrent.DataWorker
import tech.lp2p.loki.torrent.Descriptor
import tech.lp2p.loki.torrent.MagnetUri
import tech.lp2p.loki.torrent.MessageRouter
import tech.lp2p.loki.torrent.PieceStatistics
import tech.lp2p.loki.torrent.Torrent
import tech.lp2p.loki.torrent.TorrentId
import tech.lp2p.loki.torrent.TorrentState
import tech.lp2p.loki.torrent.TorrentWorker
import tech.lp2p.loki.torrent.createTorrentId
import tech.lp2p.loki.torrent.process
import java.io.File
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.URI
import java.nio.ByteBuffer
import java.nio.channels.Selector
import java.util.concurrent.ConcurrentHashMap


suspend fun downloadMagnetUri(
    root: File, magnetUri: MagnetUri,
    period: Long, listener: (TorrentState) -> Unit
) {
    val loki = Loki(root, magnetUri.torrentId)
    loki.start(magnetUri, period, listener)
}


internal const val MAX_SIMULTANEOUSLY_ASSIGNED_PIECES: Int = 3
internal const val SHA1_HASH_LENGTH: Int = 20
internal const val UT_METADATA = "ut_metadata"
internal const val UT_PEX = "ut_pex"


// MAGNET
internal const val MAX_PEER_CONNECTIONS: Int = 500
internal const val MAX_PEER_CONNECTIONS_PER_TORRENT: Int = 500
internal const val TRANSFER_BLOCK_SIZE: Int = 16 * 1024
const val MAX_IO_QUEUE_SIZE: Int = Int.MAX_VALUE
const val MAX_CONCURRENT_ACTIVE_PEERS_TORRENT: Int = 10
const val META_EXCHANGE_BLOCK_SIZE: Int = 16 * 1024 // 16 KB
const val META_EXCHANGE_MAX_SIZE: Int = 2 * 1024 * 1024 // 2 MB
const val MSE_PRIVATE_KEY_SIZE: Int = 20 // 20 bytes
const val MAX_OUTSTANDING_REQUESTS: Int = 250
const val NETWORK_BUFFER_SIZE: Int = 1024 * 1024 // 1 MB

// time constants
const val MESSAGE_PROCESSING_INTERVAL: Long = 100 // 100 ms
const val PEER_HANDSHAKE_TIMEOUT: Long = 30000 // 30 sec
const val PEER_DISCOVERY_INTERVAL: Long = 5000 // 5 sec
const val MAX_PIECE_RECEIVING_TIME: Long = 5000 // 5 sec
const val PEER_INACTIVITY_THRESHOLD: Long = 3 * 60 * 1000 // 3 min
const val TIMEOUT_PEER_BAN: Long = 60 * 1000 // 1 min

val ENCRYPTION_POLICY: EncryptionPolicy = EncryptionPolicy.PREFER_PLAINTEXT

const val UNDEFINED_TORRENT_NAME: String = "undefined"
const val SCHEME: String = "magnet"
const val INFOHASH_PREFIX: String = "urn:btih:"
const val EXTENDED_MESSAGE_ID: Byte = 20
const val HANDSHAKE_TYPE_ID = 0

private object UriParams {
    const val TORRENT_ID: String = "xt"
    const val DISPLAY_NAME: String = "dn"
    const val TRACKER_URL: String = "tr"
    const val PEER: String = "x.pe"
}

/**
 * Create a magnet URI from its' string representation in BEP-9 format.
 * Current limitations:
 * - only v1 links are supported (xt=urn:btih:&lt;info-hash&gt;)
 * - base32-encoded info hashes are not supported
 */
fun parseMagnetUri(uriString: String): MagnetUri {
    return parseMagnetUri(URI(uriString))
}

/**
 * Create a magnet URI from its' URI representation in BEP-9 format.
 * Current limitations:
 * - only v1 links are supported (xt=urn:btih:&lt;info-hash&gt;)
 * - base32-encoded info hashes are not supported
 */
private fun parseMagnetUri(uri: URI): MagnetUri {
    require(SCHEME == uri.scheme) { "Invalid scheme: " + uri.scheme }

    val paramsMap = collectParams(uri)

    val infoHashes = requiredParams(paramsMap)
        .filter { value: String -> value.startsWith(INFOHASH_PREFIX) }
        .map { value: String -> value.substring(INFOHASH_PREFIX.length) }
        .toSet()


    check(infoHashes.size == 1) {
        String.format(
            "Parameter '%s' has invalid number of values with prefix '%s': %s",
            UriParams.TORRENT_ID, INFOHASH_PREFIX, infoHashes.size
        )
    }
    val torrentId = buildTorrentId(infoHashes.iterator().next())
    val builder = MagnetUri.torrentId(torrentId)

    val displayName = optionalParams(UriParams.DISPLAY_NAME, paramsMap).firstOrNull()
    if (displayName != null) builder.name(displayName)

    optionalParams(UriParams.TRACKER_URL, paramsMap).forEach { trackerUrl: String ->
        builder.tracker(
            trackerUrl
        )
    }
    optionalParams(UriParams.PEER, paramsMap).forEach { value: String ->
        try {
            builder.peer(parsePeer(value))
        } catch (_: Exception) {
            debug(
                "MagnetUriParser", "Failed to parse peer address: $value"
            )
        }
    }

    return builder.buildUri()
}

private fun collectParams(uri: URI): Map<String, MutableList<String>> {
    val paramsMap: MutableMap<String, MutableList<String>> = HashMap()

    // magnet:?param1=value1...
    // uri.getSchemeSpecificPart() will start with the question mark and contain all name-value pairs
    var scheme = uri.schemeSpecificPart
    if (scheme.startsWith("?")) {
        scheme = scheme.substring(1)
    }
    val params = scheme.split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    for (param in params) {
        val parts = param.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        if (parts.size == 2) {
            val name = parts[0]
            val value = parts[1]
            val values = paramsMap.computeIfAbsent(name) { _: String? -> ArrayList() }
            values.add(value)
        }
    }

    return paramsMap
}

private fun requiredParams(paramsMap: Map<String, MutableList<String>>): List<String> {
    val values = paramsMap.getOrDefault(UriParams.TORRENT_ID, emptyList())
    check(values.isNotEmpty()) {
        String.format(
            "Required parameter '%s' is missing: %s",
            UriParams.TORRENT_ID, 0
        )
    }
    return values
}

private fun optionalParams(
    paramName: String,
    paramsMap: Map<String, MutableList<String>>
): List<String> {
    return paramsMap.getOrDefault(paramName, emptyList())
}

private fun buildTorrentId(infoHash: String): TorrentId {
    val bytes: ByteArray
    val len = infoHash.length
    bytes = when (len) {
        40 -> {
            fromHex(infoHash)
        }

        32 -> {
            infoHashFromBase32(infoHash)
        }

        else -> {
            throw IllegalArgumentException("Invalid info hash length: $len")
        }
    }
    return createTorrentId(bytes)
}

private fun parsePeer(value: String): InetPeerAddress {
    val parts = value.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    require(parts.size == 2) { "Invalid peer format: $value; should be <host>:<port>" }
    val hostname = parts[0]
    val port = parts[1].toInt()
    return InetPeerAddress(hostname, port)
}

internal class Loki(root: File, val torrentId: TorrentId) {
    internal val scope = CoroutineScope(Dispatchers.IO)
    internal val messageRouter = MessageRouter()
    internal val descriptor = Descriptor()
    internal val bufferedPieceRegistry = BufferedPieceRegistry()


    internal val messageDispatcher: MessageDispatcher
    internal val connectionSource: ConnectionSource
    internal val dataWorker: DataWorker
    internal val idun: Idun


    internal val storage = Storage(root)
    internal val torrentWorker = TorrentWorker(this)
    internal val connectionPool = PeerConnectionPool(scope, torrentWorker)
    internal val torrentState = TorrentState(descriptor)

    private val selector = Selector.open()
    private val dataReceiver = DataReceiver(scope, selector)

    private val peers: MutableSet<Peer> = ConcurrentHashMap.newKeySet()

    @Volatile
    internal var torrent: Torrent? = null

    @Volatile
    internal var source: ByteBuffer? = null

    @Volatile
    internal var bitfield: Bitfield? = null

    @Volatile
    internal var assignments: Assignments? = null

    @Volatile
    internal var pieceStatistics: PieceStatistics? = null

    @Volatile
    internal var bitfieldConsumer: BitfieldCollectingConsumer? = null

    init {
        val peerId = createRandomKey(SHA1_HASH_LENGTH)
        val port = nextFreePort()

        idun = newIdun(peerId, port)

        val extendedMessagesHandler: List<ExtendedMessageHandler> = listOf(
            PeerExchangeMessageHandler(),
            UtMetadataMessageHandler()
        )

        val extraHandlers: MutableMap<Byte, MessageHandler> = HashMap()
        extraHandlers[StandardBittorrentProtocol.PORT_ID] = PortMessageHandler()
        extraHandlers[EXTENDED_MESSAGE_ID] =
            ExtendedProtocol(extendedMessagesHandler)

        val bittorrentProtocol: MessageHandler = StandardBittorrentProtocol(extraHandlers)


        // add default handshake handlers to the beginning of the connection handling chain
        val handshakeHandlers = setOf(
            DHTHandshakeHandler(port),
            BitfieldConnectionHandler(this),
            ExtendedProtocolHandshakeHandler(
                this, extendedMessagesHandler, port
            )
        )

        val peerConnectionFactory = PeerConnectionFactory(
            peerId,
            selector, bufferedPieceRegistry, bittorrentProtocol,
            this, dataReceiver, handshakeHandlers
        )

        val localAddress = InetSocketAddress(
            InetAddress.getLoopbackAddress(), port
        )
        val connectionAcceptor = SocketChannelConnectionAcceptor(
            selector, peerConnectionFactory, localAddress
        )

        this.dataWorker = DataWorker(this)

        this.connectionSource = ConnectionSource(
            scope,
            peerConnectionFactory, connectionPool, connectionAcceptor
        )
        this.messageDispatcher = MessageDispatcher(scope, connectionPool, descriptor)
    }


    internal suspend fun start(
        magnetUri: MagnetUri,
        period: Long,
        listener: (TorrentState) -> Unit
    ): Unit = coroutineScope {
        try {
            startup()
            process(this@Loki, magnetUri, period, listener)
        } finally {
            shutdown()
        }
    }


    suspend fun shutdown() {
        debug("Loki", "Start Shutdown")

        descriptor.stop()

        idun.shutdown()
        connectionPool.shutdown()
        messageDispatcher.shutdown()
        connectionSource.shutdown()
        dataReceiver.shutdown()

        selector.close()

        // clear data for fast gc
        bufferedPieceRegistry.clear()
        messageRouter.clear()
        peers.clear()
        torrentWorker.clear()

        descriptor.let {
            it.dataDescriptor?.let {
                try {
                    descriptor.dataDescriptor!!.close()
                } catch (throwable: Throwable) {
                    debug("TorrentRegistry", throwable)
                }
            }
        }

        scope.cancel()
        debug("Loki", "End Shutdown")
    }

    internal fun startup() {
        debug("Loki", "Startup")
        idun.startup()

        connectionPool.startup()
        messageDispatcher.startup()
        connectionSource.startup()
        dataReceiver.startup()

        scope.launch {
            while (scope.isActive) {
                val channel = Channel<tech.lp2p.idun.core.Peer>()

                try {
                    idun.peers(torrentId.bytes, channel)

                    for (p in channel) {
                        val peer = createPeer(p.inetAddress, p.port)
                        if (!peers.contains(peer)) {
                            peers.add(peer)
                            addPeer(peer)
                        }
                    }
                } catch (throwable: Throwable) {
                    debug("Loki", throwable)
                }

                delay(PEER_DISCOVERY_INTERVAL)
            }
        }

    }

    internal fun getDescriptor(torrentId: TorrentId): Descriptor? {
        if (torrentId != this.torrentId) return null
        return descriptor
    }


    internal fun addPeer(peer: Peer) {
        try {
            require(!(peer.port < 0 || peer.port > 65535)) { "Invalid port: " + peer.port }
            debug("Loki", "add peer -> $peer")
            torrentWorker.onPeerDiscovered(peer)
        } catch (throwable: Throwable) {
            debug("Loki", throwable)
        }
    }
}


@Suppress("SameReturnValue")
internal val isDebug: Boolean
    get() = true

@Suppress("SameReturnValue")
private val isError: Boolean
    get() = true


internal fun debug(tag: String, message: String) {
    if (isDebug) {
        println("$tag $message")
    }
}


internal fun debug(tag: String, message: String, throwable: Throwable) {
    if (isError) {
        System.err.println("$tag $message")
        throwable.printStackTrace(System.err)
    }
}

internal fun debug(tag: String, throwable: Throwable) {
    if (isError) {
        System.err.println(tag + " " + throwable.localizedMessage)
        throwable.printStackTrace(System.err)
    }
}





