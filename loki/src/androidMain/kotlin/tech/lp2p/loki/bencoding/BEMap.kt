package tech.lp2p.loki.bencoding

import kotlinx.io.Buffer

/**
 * BEncoded dictionary.
 */
class BEMap(val content: ByteArray?, value: Map<String, BEObject>) :
    BEObject {

    override fun writeTo(out: Buffer) {
        encode(this, out)
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is BEMap) {
            return false
        }

        if (other === this) {
            return true
        }

        return value == other.value
    }


    override fun toString(): String {
        return value.toString()
    }

    val value: Map<String, BEObject> = value.toMap()
}
