package tech.lp2p.loki.bencoding

enum class BEType {
    STRING,
    INTEGER,
    LIST,
    MAP
}
