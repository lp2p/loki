package tech.lp2p.loki.bencoding

import kotlinx.io.Buffer

fun encode(string: BEString, out: Buffer) {
    val bytes = string.content
    encodeString(bytes, out)
}

private fun encodeString(bytes: ByteArray, out: Buffer) {
    out.write(bytes.size.toString().toByteArray(Charsets.UTF_8))
    out.writeByte(DELIMITER.code.toByte())
    out.write(bytes)
}

fun encode(integer: BEInteger, out: Buffer) {
    val value = integer.value
    out.writeByte(INTEGER_PREFIX.code.toByte())
    out.write(value.toInt().toString().toByteArray(Charsets.UTF_8))
    out.writeByte(EOF.code.toByte())
}

fun encode(list: BEList, out: Buffer) {
    val values = list.value
    out.writeByte(LIST_PREFIX.code.toByte())

    for (value in values) {
        value.writeTo(out)
    }

    out.writeByte(EOF.code.toByte())
}

fun encode(map: BEMap, out: Buffer) {

    out.writeByte(MAP_PREFIX.code.toByte())

    val mapped = mutableMapOf<ByteArray, BEObject>()
    map.value.entries.forEach { e: Map.Entry<String, BEObject> ->
        mapped.put(
            e.key.toByteArray(
                Charsets.UTF_8
            ), e.value
        )

    }
    mapped.toSortedMap(ByteStringComparator())

    for ((key, value) in mapped) {
        encodeString(key, out)
        value.writeTo(out)
    }
    out.writeByte(EOF.code.toByte())
}

