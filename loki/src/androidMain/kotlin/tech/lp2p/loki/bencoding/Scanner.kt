package tech.lp2p.loki.bencoding

import kotlinx.io.Buffer


class Scanner(private val buffer: Buffer) {

    fun read(): Int {
        if (!buffer.exhausted()) {
            return buffer.readByte().toInt() and 0xFF
        }
        return -1
    }

    fun peek(): Int {
        if (!buffer.exhausted()) {
            return buffer.peek().readByte().toInt() and 0xFF
        }
        return -1
    }

    fun readMapObject(builder: BEMapBuilder): BEMap {
        var c: Int

        while ((read().also { c = it }) != -1) {
            if (!builder.accept(c)) {
                break
            }
        }
        return builder.build() as BEMap
    }
}
