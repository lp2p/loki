package tech.lp2p.loki.bencoding

import kotlinx.io.Buffer
import java.math.BigInteger

/**
 * BEncoded integer.
 *
 *
 * «BEP-3: The BitTorrent Protocol Specification» defines integers
 * as unsigned numeric values with an arbitrary number of digits.
 */
data class BEInteger(val value: BigInteger) : BEObject {

    override fun writeTo(out: Buffer) {
        encode(this, out)
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is BEInteger) {
            return false
        }

        if (other === this) {
            return true
        }

        return value == other.value
    }


    override fun toString(): String {
        return value.toString(10)
    }
}
