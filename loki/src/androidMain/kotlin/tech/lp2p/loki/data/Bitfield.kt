package tech.lp2p.loki.data

import kotlinx.atomicfu.locks.ReentrantLock
import tech.lp2p.loki.protocol.BitOrder
import tech.lp2p.loki.protocol.reverseBits
import java.util.BitSet

class Bitfield {

    /**
     * Bitmask indicating availability of pieces.
     * If the n-th bit is set, then the n-th piece is complete and verified.
     */
    private val bitmask: BitSet

    /**
     * Total number of pieces in torrent.
     */
    val piecesTotal: Int

    private val lock: ReentrantLock

    /**
     * Bitmask indicating pieces that should be skipped.
     * If the n-th bit is set, then the n-th piece should be skipped.
     */
    @Volatile
    private var skipped: BitSet? = null


    constructor(piecesTotal: Int) {
        this.piecesTotal = piecesTotal
        this.bitmask = BitSet(piecesTotal)
        this.lock = ReentrantLock()
    }

    /**
     * Creates bitfield based on a bitmask.
     * Used for creating peers' bitfields.
     *
     * @param value       Bitmask that describes status of all pieces.
     * If position i is set to 1, then piece with index i is complete and verified.
     * @param piecesTotal Total number of pieces in threads.torrent.
     */
    constructor(value: ByteArray, bitOrder: BitOrder, piecesTotal: Int) {
        this.piecesTotal = piecesTotal
        this.bitmask = createBitmask(value, bitOrder, piecesTotal)
        this.lock = ReentrantLock()
    }

    /**
     * @return Bitmask that describes status of all pieces.
     * If the n-th bit is set, then the n-th piece
     * is in [PieceStatus.COMPLETE_VERIFIED] status.
     */
    fun getBitmask(): BitSet {
        lock.lock()
        try {
            return bitmask.clone() as BitSet
        } finally {
            lock.unlock()
        }
    }

    /**
     * @param bitOrder Order of bits to use to create the byte array
     * @return Bitmask that describes status of all pieces.
     * If the n-th bit is set, then the n-th piece
     * is in [PieceStatus.COMPLETE_VERIFIED] status.
     */
    fun toByteArray(bitOrder: BitOrder): ByteArray {
        var bytes: ByteArray
        val truncated: Boolean

        lock.lock()
        try {
            bytes = bitmask.toByteArray()
            truncated = (bitmask.length() < piecesTotal)
        } finally {
            lock.unlock()
        }

        if (bitOrder == BitOrder.LITTLE_ENDIAN) {
            bytes = reverseBits(bytes)
        }
        if (truncated) {
            val arr = ByteArray(getBitmaskLength(piecesTotal))
            System.arraycopy(bytes, 0, arr, 0, bytes.size)
            return arr
        } else {
            return bytes
        }
    }

    val piecesComplete: Int
        get() {
            lock.lock()
            try {
                return bitmask.cardinality()
            } finally {
                lock.unlock()
            }
        }

    val piecesRemaining: Int
        get() {
            lock.lock()
            try {
                if (skipped == null) {
                    return piecesTotal - piecesComplete
                } else {
                    val bitmask = getBitmask()
                    bitmask.or(skipped!!)
                    return piecesTotal - bitmask.cardinality()
                }
            } finally {
                lock.unlock()
            }
        }


    fun getPieceStatus(pieceIndex: Int): PieceStatus {
        validatePieceIndex(pieceIndex)

        val status: PieceStatus

        val verified: Boolean
        lock.lock()
        try {
            verified = bitmask[pieceIndex]
        } finally {
            lock.unlock()
        }

        status = if (verified) {
            PieceStatus.COMPLETE_VERIFIED
        } else {
            PieceStatus.INCOMPLETE
        }

        return status
    }

    /**
     * Shortcut method to find out if the piece has been downloaded.
     *
     * @param pieceIndex Piece index (0-based)
     * @return true if the piece has been downloaded
     */
    fun isComplete(pieceIndex: Int): Boolean {
        val pieceStatus = getPieceStatus(pieceIndex)
        return (pieceStatus == PieceStatus.COMPLETE || pieceStatus == PieceStatus.COMPLETE_VERIFIED)
    }

    /**
     * Shortcut method to find out if the piece has been downloaded and verified.
     *
     * @param pieceIndex Piece index (0-based)
     * @return true if the piece has been downloaded and verified
     */
    fun isVerified(pieceIndex: Int): Boolean {
        val pieceStatus = getPieceStatus(pieceIndex)
        return pieceStatus == PieceStatus.COMPLETE_VERIFIED
    }

    /**
     * Mark piece as complete and verified.
     *
     * @param pieceIndex Piece index (0-based)
     */
    fun markVerified(pieceIndex: Int) {
        assertChunkComplete(pieceIndex)

        lock.lock()
        try {
            bitmask.set(pieceIndex)
        } finally {
            lock.unlock()
        }
    }

    private fun assertChunkComplete(pieceIndex: Int) {
        validatePieceIndex(pieceIndex)
    }

    private fun validatePieceIndex(pieceIndex: Int) {
        if (pieceIndex < 0 || pieceIndex >= piecesTotal) {
            throw RuntimeException(
                "Illegal piece index: " + pieceIndex +
                        ", expected 0.." + (piecesTotal - 1)
            )
        }
    }

    /**
     * Mark a piece as skipped
     */
    fun skip(pieceIndex: Int) {
        validatePieceIndex(pieceIndex)

        lock.lock()
        try {
            if (skipped == null) {
                skipped = BitSet(piecesTotal)
            }
            skipped!!.set(pieceIndex)
        } finally {
            lock.unlock()
        }
    }

    /**
     * Status of a particular piece.
     */
    enum class PieceStatus {
        INCOMPLETE, COMPLETE, COMPLETE_VERIFIED
    }
}
