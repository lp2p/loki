package tech.lp2p.loki.data

import tech.lp2p.loki.protocol.BitOrder
import tech.lp2p.loki.protocol.isSet
import tech.lp2p.loki.protocol.reverseBits
import java.util.BitSet
import kotlin.math.ceil


fun buildChunkDescriptor(data: DataRange, blockSize: Long, checksum: ByteArray):
        ChunkDescriptor {
    val blockSet = createMutableBlockSet(data.length(), blockSize)

    return ChunkDescriptor(data, blockSet, checksum)
}


fun createBitmask(bytes: ByteArray, bitOrder: BitOrder, piecesTotal: Int): BitSet {
    var data = bytes
    val expectedBitmaskLength = getBitmaskLength(piecesTotal)
    require(data.size == expectedBitmaskLength) {
        "Invalid bitfield: total (" + piecesTotal +
                "), bitmask length (" + data.size + "). Expected bitmask length: " + expectedBitmaskLength
    }

    if (bitOrder == BitOrder.LITTLE_ENDIAN) {
        data = reverseBits(data)
    }

    val bitmask = BitSet(piecesTotal)
    for (i in 0 until piecesTotal) {
        if (isSet(data, BitOrder.BIG_ENDIAN, i)) {
            bitmask.set(i)
        }
    }
    return bitmask
}

fun getBitmaskLength(piecesTotal: Int): Int {
    return ceil(piecesTotal / 8.0).toInt()
}


fun createReadWriteDataRange(
    units: List<StorageUnit>,
    offsetInFirstUnit: Long,
    limitInLastUnit: Long
): DataRange {
    return createReadWriteDataRange(
        units,
        offsetInFirstUnit,
        units.size - 1,
        limitInLastUnit
    )
}

private fun createReadWriteDataRange(
    units: List<StorageUnit>,
    offsetInFirstUnit: Long,
    lastUnit: Int,
    limitInLastUnit: Long
): DataRange {
    require(units.isNotEmpty()) { "Empty list of units" }

    require(!(lastUnit < 0 || lastUnit > units.size - 1)) { "Invalid last unit index: " + lastUnit + ", expected 0.." + (units.size - 1) }
    require(!(offsetInFirstUnit < 0 || offsetInFirstUnit > units[0].capacity() - 1)) {
        "Invalid offset in first unit: " + offsetInFirstUnit +
                ", expected 0.." + (units[0].capacity() - 1)
    }
    require(!(limitInLastUnit <= 0 || limitInLastUnit > units[lastUnit].capacity())) {
        "Invalid limit in last unit: " + limitInLastUnit +
                ", expected 1.." + (units[lastUnit].capacity())
    }
    require(!(0 == lastUnit && offsetInFirstUnit >= limitInLastUnit)) {
        "Offset is greater than limit in a single-unit range: " +
                offsetInFirstUnit + " >= " + limitInLastUnit
    }

    val length = calculateLength(units, offsetInFirstUnit, limitInLastUnit)
    return DataRange(
        units, lastUnit, offsetInFirstUnit,
        limitInLastUnit, length, calculateOffsets(units, offsetInFirstUnit)
    )
}

private fun calculateLength(
    units: List<StorageUnit>,
    offsetInFirstUnit: Long,
    limitInLastUnit: Long
): Long {
    var size: Long
    if (units.size == 1) {
        size = limitInLastUnit - offsetInFirstUnit
    } else {
        size = units[0].capacity() - offsetInFirstUnit
        for (i in 1 until units.size - 1) {
            size += units[i].capacity()
        }
        size += limitInLastUnit
    }
    return size
}

private fun calculateOffsets(units: List<StorageUnit>, offsetInFirstUnit: Long): LongArray {
    val fileOffsets = LongArray(units.size)
    // first "virtual" address (first file begins with offset 0)
    fileOffsets[0] = 0
    if (units.size > 1) {
        // it's possible that chunk does not have access to the entire first file
        fileOffsets[1] = units[0].capacity() - offsetInFirstUnit
    }
    for (i in 2 until units.size) {
        fileOffsets[i] = fileOffsets[i - 1] + units[i - 1].capacity()
    }
    return fileOffsets
}


fun createMutableBlockSet(length: Long, blockSize: Long): MutableBlockSet {
    // intentionally allow length to be greater than block size
    require(!(length < 0 || blockSize < 0)) {
        "Illegal arguments: length ($length), block size ($blockSize)"
    }

    val blockCount = ceil((length.toDouble()) / blockSize).toLong()
    require(blockCount <= Int.MAX_VALUE) {
        "Too many blocks: length (" + length +
                "), block size (" + blockSize + "), total blocks (" + blockCount + ")"
    }

    // handle the case when the last block is smaller than the others
    var lastBlockSize = length % blockSize
    val lastBlockOffset: Long
    if (lastBlockSize > 0) {
        lastBlockOffset = length - lastBlockSize
    } else {
        lastBlockSize = blockSize
        lastBlockOffset = length - blockSize
    }
    return MutableBlockSet(
        length, blockSize, blockCount.toInt(), lastBlockSize,
        lastBlockOffset, BitSet(blockCount.toInt())
    )
}

