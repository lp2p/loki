package tech.lp2p.loki.data

data class ChunkDescriptor(
    val data: DataRange,
    val blockSet: MutableBlockSet,
    val checksum: ByteArray
) {
    fun blockCount(): Int {
        return blockSet.blockCount
    }

    fun blockSize(): Long {
        return blockSet.blockSize
    }

    fun isPresent(blockIndex: Int): Boolean {
        return blockSet.isPresent(blockIndex)
    }

    val isComplete: Boolean
        get() = blockSet.isComplete

    fun clear() {
        blockSet.clear()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ChunkDescriptor

        if (data != other.data) return false
        if (blockSet != other.blockSet) return false
        if (!checksum.contentEquals(other.checksum)) return false
        if (isComplete != other.isComplete) return false

        return true
    }

    override fun hashCode(): Int {
        var result = data.hashCode()
        result = 31 * result + blockSet.hashCode()
        result = 31 * result + checksum.contentHashCode()
        result = 31 * result + isComplete.hashCode()
        return result
    }
}
