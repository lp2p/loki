package tech.lp2p.loki.data

import org.kotlincrypto.hash.sha1.SHA1
import java.nio.ByteBuffer


private fun digest(data: DataRange): ByteArray? {
    val digest = SHA1()
    var buffer = ByteBuffer.allocate(data.length.toInt())
    data.visitUnits(object : DataRangeVisitor {
        override fun visitUnit(unit: StorageUnit, off: Long, lim: Long) {
            unit.readBuffer(buffer, off)
        }
    })

    if (!buffer.hasRemaining()) {
        buffer.flip()
        digest.update(buffer)
        return digest.digest()
    }
    return null
}


fun verify(chunk: ChunkDescriptor): Boolean {
    val expected = chunk.checksum
    val actual = digest(chunk.data)
    actual?.let {
        return expected.contentEquals(actual)
    }
    return false
}


