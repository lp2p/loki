package tech.lp2p.loki.crypto

import java.nio.ByteBuffer
import javax.crypto.Cipher

fun mutateBuffer(cipher: Cipher, buffer: ByteBuffer) {
    if (buffer.hasRemaining()) {
        val position = buffer.position()
        var bytes = ByteArray(buffer.remaining())
        buffer[bytes]
        buffer.position(position)
        bytes = cipher.update(bytes)
        buffer.put(bytes)
    }
}

