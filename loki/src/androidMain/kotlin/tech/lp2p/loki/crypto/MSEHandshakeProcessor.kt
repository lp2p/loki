package tech.lp2p.loki.crypto

import org.kotlincrypto.hash.sha1.SHA1
import tech.lp2p.loki.ENCRYPTION_POLICY
import tech.lp2p.loki.Loki
import tech.lp2p.loki.MSE_PRIVATE_KEY_SIZE
import tech.lp2p.loki.buffer.DelegatingByteBufferView
import tech.lp2p.loki.debug
import tech.lp2p.loki.net.ByteChannelReader
import tech.lp2p.loki.net.Peer
import tech.lp2p.loki.net.decodeUnsigned
import tech.lp2p.loki.net.encodeUnsigned
import tech.lp2p.loki.protocol.DecodingResult
import tech.lp2p.loki.protocol.EncryptionPolicy
import tech.lp2p.loki.protocol.Handshake
import tech.lp2p.loki.protocol.MSECipher
import tech.lp2p.loki.protocol.MessageHandler
import tech.lp2p.loki.protocol.getShortBytes
import tech.lp2p.loki.torrent.TorrentId
import java.nio.ByteBuffer
import java.nio.channels.ByteChannel
import java.nio.channels.ReadableByteChannel
import kotlin.random.Random
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

internal class MSEHandshakeProcessor(
    private val loki: Loki,
    protocol: MessageHandler
) {
    private val keyGenerator: MSEKeyPairGenerator
    private val protocol: MessageHandler

    private val localEncryptionPolicy = ENCRYPTION_POLICY

    // indicates, that MSE encryption negotiation procedure should not be used
    private val mseDisabled: Boolean

    init {
        val msePrivateKeySize = MSE_PRIVATE_KEY_SIZE
        val mseDisabled = !MSECipher.isKeySizeSupported(msePrivateKeySize)
        if (mseDisabled) {
            var message = "Current Bt runtime is configured to use private" +
                    " key size of " + msePrivateKeySize + " bytes for Message Stream Encryption (MSE)," +
                    " and the preferred encryption policy is " + localEncryptionPolicy.name + ". " +
                    "The aforementioned key size is not allowed in the current JDK configuration. " +
                    "Hence, MSE encryption negotiation procedure will NOT be used"


            val postfix = (" To fix this problem, please do one of the following:"
                    + " (a) update your JDK or Java runtime environment settings for unlimited cryptography support;"
                    + " (b) specify a different private key size (not recommended)")

            when (localEncryptionPolicy) {
                EncryptionPolicy.REQUIRE_PLAINTEXT, EncryptionPolicy.PREFER_PLAINTEXT, EncryptionPolicy.PREFER_ENCRYPTED -> {
                    message += (", and all peer connections will be established in plaintext by using the standard BitTorrent handshake."
                            + " This may negatively affect the number of peers, which can be connected to."
                            + postfix)
                    debug("MSEHandshakeProcessor", message)
                }

                EncryptionPolicy.REQUIRE_ENCRYPTED -> {
                    message += (", and considering the requirement for mandatory encryption, this effectively means,"
                            + " that no peer connections will ever be established."
                            + postfix
                            + "; (c) choose a more permissive encryption policy")
                    throw IllegalStateException(message)
                }

            }
        }
        this.mseDisabled = mseDisabled

        this.keyGenerator = MSEKeyPairGenerator(msePrivateKeySize)
        this.protocol = protocol
    }

    private fun assertPolicyIsCompatible() {
        if (!localEncryptionPolicy.isCompatible(EncryptionPolicy.REQUIRE_PLAINTEXT)) {
            throw RuntimeException(
                ("Encryption policies are incompatible: peer's (" + EncryptionPolicy.REQUIRE_PLAINTEXT.name
                        + "), local (" + localEncryptionPolicy.name + ")")
            )
        }
    }

    fun negotiateOutgoing(
        channel: ByteChannel,
        inputBuffer: ByteBuffer, // todo remove
        outputBuffer: ByteBuffer // todo remove
    ): MSECipher? {
        if (mseDisabled) {
            return null
        }


        /*
         * Blocking steps:
         *
         * 1. A->B: Diffie Hellman Ya, PadA
         * 2. B->A: Diffie Hellman Yb, PadB
         * 3. A->B:
         *  - HASH('req1', S),
         *  - HASH('req2', SKEY) xor HASH('req3', S),
         *  - ENCRYPT(VC, crypto_provide, len(PadC), PadC, len(IA)),
         *  - ENCRYPT(IA)
         * 4. B->A:
         * - ENCRYPT(VC, crypto_select, len(padD), padD),
         * - ENCRYPT2(Payload Stream)
         * 5. A->B: ENCRYPT2(Payload Stream)
         */
        val reader = reader(channel)

        // 1. A->B: Diffie Hellman Ya, PadA
        // send our public key
        val keys = keyGenerator.generateKeys()
        outputBuffer.put(keys.publicKey.encoded)
        outputBuffer.put(padding)
        outputBuffer.flip()
        channel.write(outputBuffer)
        outputBuffer.clear()

        // 2. B->A: Diffie Hellman Yb, PadB
        // receive peer's public key
        val phase1Min = PUBLIC_KEY_BYTES
        val phase1Limit = phase1Min + PADDING_MAX_LENGTH
        val phase1Read = reader.readBetween(phase1Min, phase1Limit).read(inputBuffer)
        inputBuffer.flip()
        val peerPublicKey = decodeUnsigned(inputBuffer, phase1Min)
        inputBuffer.clear() // discard the padding, if present

        // calculate shared secret S
        val sharedSecrets = calculateSharedSecret(peerPublicKey, keys.privateKey)

        // 3. A->B:
        val digest = SHA1()
        // - HASH('req1', S)
        digest.update("req1".toByteArray(Charsets.US_ASCII))
        digest.update(encodeUnsigned(sharedSecrets, PUBLIC_KEY_BYTES))
        outputBuffer.put(digest.digest())
        // - HASH('req2', SKEY) xor HASH('req3', S)
        digest.update("req2".toByteArray(Charsets.US_ASCII))
        digest.update(loki.torrentId.bytes)
        val b1 = digest.digest()
        digest.update("req3".toByteArray(Charsets.US_ASCII))
        digest.update(encodeUnsigned(sharedSecrets, PUBLIC_KEY_BYTES))
        val b2 = digest.digest()
        outputBuffer.put(xor(b1, b2))
        // write
        outputBuffer.flip()
        channel.write(outputBuffer)
        outputBuffer.clear()

        val sbytes = encodeUnsigned(sharedSecrets, PUBLIC_KEY_BYTES)
        val cipher = MSECipher.forInitiator(sbytes, loki.torrentId)
        val encryptedChannel: ByteChannel =
            EncryptedChannel(channel, cipher.decryptionCipher, cipher.encryptionCipher)
        // - ENCRYPT(VC, crypto_provide, len(PadC), PadC, len(IA))
        outputBuffer.put(VC_RAW_BYTES)
        outputBuffer.put(getCryptoProvideBitfield(localEncryptionPolicy))
        val padding = zeroPadding
        outputBuffer.put(getShortBytes(padding.size))
        outputBuffer.put(padding)
        // - ENCRYPT(IA)
        // do not write IA (initial payload data) for now, wait for encryption negotiation
        outputBuffer.putShort(0.toShort()) // IA length = 0
        outputBuffer.flip()
        encryptedChannel.write(outputBuffer)
        outputBuffer.clear()

        // 4. B->A:
        // - ENCRYPT(VC, crypto_select, len(padD), padD)
        var encryptedVC: ByteArray
        run {
            val throwawayCipher = MSECipher.forInitiator(sbytes, loki.torrentId)
            encryptedVC = throwawayCipher.decryptionCipher.doFinal(VC_RAW_BYTES)
        }
        // synchronize on the incoming stream of data
        val phase2Min = encryptedVC.size + 4 /*crypto_select*/ + 2 /*padding_len*/
        // account for (phase1Limit - phase1Read) in case the padding from phase1 arrives later than expected
        val phase2Limit = (phase1Limit - phase1Read) + phase2Min + PADDING_MAX_LENGTH

        val initpos = inputBuffer.position()
        // use plaintext reader because encryption stream is not synced yet and will potentially produce garbage
        val phase2Read = reader.readBetween(phase2Min, phase2Limit).sync(inputBuffer, encryptedVC)
        val matchpos = inputBuffer.position()

        // the rest of the data is known to be encrypted
        val encryptedReader = reader(encryptedChannel)
        // but we need to align the incoming (decrypting) cipher
        // for the number of encrypted bytes that have already arrived
        // and decrypt these bytes in the incoming data buffer for later processing
        inputBuffer.limit(initpos + phase2Read)
        run {
            cipher.decryptionCipher.update(ByteArray(VC_RAW_BYTES.size))
            val encryptedData = ByteArray(inputBuffer.remaining())
            inputBuffer[encryptedData]
            inputBuffer.position(matchpos)
            val decryptedData = cipher.decryptionCipher.update(encryptedData)
            inputBuffer.put(decryptedData)
            inputBuffer.position(matchpos)
        }

        // we may still need to receive some handshake data (e.g. padding), that is arriving later than expected
        if (inputBuffer.remaining() < (phase2Min - encryptedVC.size)) {
            val lim = inputBuffer.limit()
            inputBuffer.limit(inputBuffer.capacity())
            val read = encryptedReader.readAtLeast(phase2Min - encryptedVC.size)
                .readNoMoreThan((phase2Min - encryptedVC.size) + PADDING_MAX_LENGTH)
                .read(inputBuffer)
            inputBuffer.position(matchpos)
            inputBuffer.limit(lim + read)
        }

        val cryptoSelect = ByteArray(4)
        inputBuffer[cryptoSelect]
        val negotiatedEncryptionPolicy = selectPolicy(cryptoSelect, localEncryptionPolicy)


        val theirPadding = inputBuffer.getShort().toInt() and 0xFFFF
        val missing = (theirPadding - inputBuffer.remaining())
        if (missing > 0) {
            val pos = inputBuffer.position()
            inputBuffer.limit(inputBuffer.capacity())
            encryptedReader.readAtLeast(missing).read(inputBuffer)
            inputBuffer.flip()
            inputBuffer.position(pos)
        }

        // account for the upper layer protocol data that has already arrived
        inputBuffer.position(inputBuffer.position() + theirPadding)
        inputBuffer.compact()
        outputBuffer.clear()

        // - ENCRYPT2(Payload Stream)
        return when (negotiatedEncryptionPolicy) {
            EncryptionPolicy.REQUIRE_PLAINTEXT, EncryptionPolicy.PREFER_PLAINTEXT -> {
                null
            }

            EncryptionPolicy.PREFER_ENCRYPTED, EncryptionPolicy.REQUIRE_ENCRYPTED -> {
                cipher
            }
        }
    }

    fun negotiateIncoming(
        peer: Peer,
        channel: ByteChannel,
        inBuffer: ByteBuffer, // todo remove
        outBuffer: ByteBuffer // todo remove
    ): MSECipher? {
        if (mseDisabled) {
            return null
        }

        /*
         * Blocking steps:
         *
         * 1. A->B: Diffie Hellman Ya, PadA
         * 2. B->A: Diffie Hellman Yb, PadB
         * 3. A->B:
         *  - HASH('req1', S),
         *  - HASH('req2', SKEY) xor HASH('req3', S),
         *  - ENCRYPT(VC, crypto_provide, len(PadC), PadC, len(IA)),
         *  - ENCRYPT(IA)
         * 4. B->A:
         *  - ENCRYPT(VC, crypto_select, len(padD), padD),
         *  - ENCRYPT2(Payload Stream)
         * 5. A->B: ENCRYPT2(Payload Stream)
         */
        val reader = reader(channel)

        // 1. A->B: Diffie Hellman Ya, PadA
        // receive initiator's public key
        // specify lower threshold on the amount of bytes to receive,
        // as we will try to decode plaintext message of an unknown length first
        val phase0Min = 20
        val phase0Read = reader.readAtLeast(phase0Min).read(inBuffer)
        inBuffer.flip()
        // try to determine the protocol from the first received bytes

        var result: DecodingResult? = null
        var consumed = 0
        try {
            result = protocol.decode(peer, DelegatingByteBufferView(inBuffer))
            consumed = result.consumed
        } catch (_: Exception) {
            // ignore
        }

        if (consumed > 0 && result!!.message is Handshake) {
            // decoding was successful, can use plaintext (if supported)
            assertPolicyIsCompatible()
            return null
        }

        val phase1Min = PUBLIC_KEY_BYTES
        val phase1Limit = phase1Min + PADDING_MAX_LENGTH
        inBuffer.limit(inBuffer.capacity())
        inBuffer.position(phase0Read)
        // verify that there is a sufficient amount of bytes to decode peer's public key
        val phase1Read = if (phase0Read < phase1Min) {
            reader.readAtLeast(phase1Min - phase0Read).readNoMoreThan(phase1Limit - phase0Read)
                .read(inBuffer)
        } else {
            0
        }

        inBuffer.flip()
        val peerPublicKey = decodeUnsigned(inBuffer, PUBLIC_KEY_BYTES)
        inBuffer.clear() // discard padding

        // 2. B->A: Diffie Hellman Yb, PadB
        // send our public key
        val keys = keyGenerator.generateKeys()
        outBuffer.put(keys.publicKey.encoded)
        outBuffer.put(padding)
        outBuffer.flip()
        channel.write(outBuffer)
        outBuffer.clear()

        // calculate shared secret S
        val sharedSecrets = calculateSharedSecret(peerPublicKey, keys.privateKey)

        // 3. A->B:
        val phase2Min = 20 + 20 + 8 + 4 + 2 + 2
        val phase2Limit = 20 + 20 + 8 + 4 + 2 + 512 + 2
        val digest = SHA1()
        // padding from phase 1 may be arriving later than expected, so we need to synchronize
        // on the incoming stream of data, looking for a correct S hash
        val bytes = ByteArray(20)
        // - HASH('req1', S)
        digest.update("req1".toByteArray(Charsets.US_ASCII))
        digest.update(encodeUnsigned(sharedSecrets, PUBLIC_KEY_BYTES))
        val req1hash = digest.digest()
        // syncing will also ensure that the peer knows S (otherwise synchronization will fail due to not finding the pattern)
        val phase2Read = reader.readAtLeast(phase2Min)
            .readNoMoreThan(phase2Limit + (phase1Limit - (phase0Read + phase1Read))) // account for padding arriving later
            .sync(inBuffer, req1hash)
        inBuffer.limit(phase1Read + phase2Read)

        // - HASH('req2', SKEY) xor HASH('req3', S)
        inBuffer[bytes] // read SKEY/S hash
        var requestedTorrent: TorrentId? = null
        digest.update("req3".toByteArray(Charsets.US_ASCII))
        digest.update(encodeUnsigned(sharedSecrets, PUBLIC_KEY_BYTES))
        val b2 = digest.digest()

        digest.update("req2".toByteArray(Charsets.US_ASCII))
        digest.update(loki.torrentId.bytes)
        val b1 = digest.digest()
        if (xor(b1, b2).contentEquals(bytes)) {
            requestedTorrent = loki.torrentId
        }

        // check that threads.torrent is supported and active
        checkNotNull(requestedTorrent) { "Unsupported torrent requested" }
        val descriptor = loki.descriptor
        check(descriptor.isActive()) { "Inactive torrent requested: $requestedTorrent" }

        val sbytes = encodeUnsigned(sharedSecrets, PUBLIC_KEY_BYTES)
        val cipher = MSECipher.forReceiver(sbytes, requestedTorrent)
        val encryptedChannel: ByteChannel =
            EncryptedChannel(channel, cipher.decryptionCipher, cipher.encryptionCipher)
        val encryptedReader = reader(encryptedChannel)

        // - ENCRYPT(VC, crypto_provide, len(PadC), PadC, len(IA))
        // derypt encrypted leftovers from step #3
        val pos = inBuffer.position()
        val leftovers = ByteArray(inBuffer.remaining())
        inBuffer[leftovers]
        inBuffer.position(pos)
        inBuffer.put(cipher.decryptionCipher.update(leftovers))
        inBuffer.position(pos)

        val theirVC = ByteArray(8)
        inBuffer[theirVC]
        check(VC_RAW_BYTES.contentEquals(theirVC)) { "Invalid VC: " + theirVC.contentToString() }

        val cryptoProvide = ByteArray(4)
        inBuffer[cryptoProvide]
        val negotiatedEncryptionPolicy = selectPolicy(cryptoProvide, localEncryptionPolicy)


        val theirPadding = inBuffer.getShort().toInt() and 0xFFFF
        check(theirPadding <= 512) { "Padding is too long: $theirPadding" }
        val position = inBuffer.position() // mark
        // check if the whole padding block has already arrived
        if (inBuffer.remaining() < theirPadding) {
            inBuffer.limit(inBuffer.capacity())
            inBuffer.position(phase1Read + phase2Read)
            encryptedReader.readAtLeast(theirPadding - inBuffer.remaining() + 2 /*IA length*/)
                .read(inBuffer)
            inBuffer.flip()
            inBuffer.position(position) // reset
        }

        inBuffer.position(position + theirPadding) // discard padding
        // Initial Payload length (0..65535 bytes)
        inBuffer.getShort() // currently we ignore IA, it will be processed by the upper layer
        inBuffer.compact()

        // 4. B->A:
        // - ENCRYPT(VC, crypto_select, len(padD), padD)
        // - ENCRYPT2(Payload Stream)
        outBuffer.put(VC_RAW_BYTES)
        outBuffer.put(getCryptoProvideBitfield(negotiatedEncryptionPolicy))
        val padding = zeroPadding
        outBuffer.putShort(padding.size.toShort())
        outBuffer.put(padding)
        outBuffer.flip()
        encryptedChannel.write(outBuffer)
        outBuffer.clear()

        return when (negotiatedEncryptionPolicy) {
            EncryptionPolicy.REQUIRE_PLAINTEXT, EncryptionPolicy.PREFER_PLAINTEXT -> {
                null
            }

            EncryptionPolicy.PREFER_ENCRYPTED, EncryptionPolicy.REQUIRE_ENCRYPTED -> {
                cipher
            }
        }
    }
}

private val receiveTimeout: Duration = 10.toDuration(DurationUnit.SECONDS)
private val waitBetweenReads: Duration = 1.toDuration(DurationUnit.SECONDS)
private const val PADDING_MAX_LENGTH = 512
private val VC_RAW_BYTES = ByteArray(8)

private fun reader(channel: ReadableByteChannel): ByteChannelReader {
    return ByteChannelReader.forChannel(channel, receiveTimeout, waitBetweenReads)
}

private val padding: ByteArray
    get() {

        val padding =
            ByteArray(Random.nextInt(PADDING_MAX_LENGTH + 1))
        for (i in padding.indices) {
            padding[i] = Random.nextInt(256).toByte()
        }
        return padding
    }

private val zeroPadding: ByteArray
    get() {
        return ByteArray(Random.nextInt(512 + 1))
    }


private fun xor(b1: ByteArray, b2: ByteArray): ByteArray {
    check(b1.size == b2.size) { "Lengths do not match: " + b1.size + ", " + b2.size }
    val result = ByteArray(b1.size)
    for (i in b1.indices) {
        result[i] = (b1[i].toInt() xor b2[i].toInt()).toByte()
    }
    return result
}

private fun getCryptoProvideBitfield(encryptionPolicy: EncryptionPolicy): ByteArray {
    val cryptoProvide = ByteArray(4)
    when (encryptionPolicy) {
        EncryptionPolicy.REQUIRE_PLAINTEXT -> cryptoProvide[3] = 1
        EncryptionPolicy.PREFER_PLAINTEXT,
        EncryptionPolicy.PREFER_ENCRYPTED -> cryptoProvide[3] = 3

        EncryptionPolicy.REQUIRE_ENCRYPTED -> cryptoProvide[3] = 2
    }
    return cryptoProvide
}

private fun selectPolicy(
    cryptoProvide: ByteArray,
    localEncryptionPolicy: EncryptionPolicy
): EncryptionPolicy {
    val plaintextProvided = (cryptoProvide[3].toInt() and 0x01) == 0x01
    val encryptionProvided = (cryptoProvide[3].toInt() and 0x02) == 0x02

    debug(
        "MSEHandshake",
        "plaintext provider $plaintextProvided encryptionProvided $encryptionProvided"
    )

    var selected: EncryptionPolicy? = null
    if (plaintextProvided || encryptionProvided) {
        when (localEncryptionPolicy) {
            EncryptionPolicy.REQUIRE_PLAINTEXT -> {
                if (plaintextProvided) {
                    selected = EncryptionPolicy.REQUIRE_PLAINTEXT
                }
            }

            EncryptionPolicy.PREFER_PLAINTEXT -> selected =
                if (plaintextProvided) EncryptionPolicy.REQUIRE_PLAINTEXT else EncryptionPolicy.REQUIRE_ENCRYPTED

            EncryptionPolicy.PREFER_ENCRYPTED -> selected =
                if (encryptionProvided) EncryptionPolicy.REQUIRE_ENCRYPTED else EncryptionPolicy.REQUIRE_PLAINTEXT

            EncryptionPolicy.REQUIRE_ENCRYPTED -> {
                if (encryptionProvided) {
                    selected = EncryptionPolicy.REQUIRE_ENCRYPTED
                }
            }
        }
    }

    checkNotNull(selected) {
        "Failed to negotiate the encryption policy: local policy (" +
                localEncryptionPolicy.name + "), peer's policy (" + cryptoProvide.contentToString() + ")"
    }
    return selected
}