package tech.lp2p.idun.tasks

enum class CountedStat {
    SENT,
    RECEIVED,
    STALLED,
    FAILED,
    SENT_SINCE_RECEIVE
}
