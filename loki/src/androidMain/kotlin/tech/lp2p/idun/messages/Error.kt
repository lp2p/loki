package tech.lp2p.idun.messages

import kotlinx.io.Buffer
import tech.lp2p.idun.core.Address
import tech.lp2p.idun.core.DHT_VERSION
import tech.lp2p.idun.core.encodeInto


data class Error(
    override val address: Address,
    override val id: ByteArray,
    override val tid: ByteArray,
    val code: Int,
    val message: String
) : Message {

    override fun encode(buffer: Buffer) {


        val base: MutableMap<String, Any> = mutableMapOf()

        // transaction ID
        base["t"] = tid
        // version
        base["v"] = DHT_VERSION
        // message type
        base["y"] = "e"


        val errorDetails: MutableList<Any> = ArrayList(2)
        errorDetails.add(code)
        errorDetails.add(message)
        base["e"] = errorDetails

        encodeInto(base, buffer)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Error

        if (code != other.code) return false
        if (address != other.address) return false
        if (!id.contentEquals(other.id)) return false
        if (!tid.contentEquals(other.tid)) return false
        if (message != other.message) return false

        return true
    }

    override fun hashCode(): Int {
        var result = code
        result = 31 * result + address.hashCode()
        result = 31 * result + id.contentHashCode()
        result = 31 * result + tid.contentHashCode()
        result = 31 * result + message.hashCode()
        return result
    }
}

