package tech.lp2p.idun.messages

import kotlinx.io.Buffer
import tech.lp2p.idun.core.Address
import tech.lp2p.idun.core.DHT_VERSION
import tech.lp2p.idun.core.encodeInto

data class PingRequest(
    override val address: Address,
    override val id: ByteArray,
    override val tid: ByteArray
) : Request {

    override fun encode(buffer: Buffer) {
        val base: MutableMap<String, Any> = mutableMapOf()
        base["a"] = mapOf<String, Any>("id" to id)

        // transaction ID
        base["t"] = tid
        // version
        base["v"] = DHT_VERSION
        // message type
        base["y"] = "q"

        // message method
        base["q"] = "ping"

        encodeInto(base, buffer)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PingRequest

        if (address != other.address) return false
        if (!id.contentEquals(other.id)) return false
        if (!tid.contentEquals(other.tid)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = address.hashCode()
        result = 31 * result + id.contentHashCode()
        result = 31 * result + tid.contentHashCode()
        return result
    }

}
