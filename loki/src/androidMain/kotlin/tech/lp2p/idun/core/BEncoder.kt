package tech.lp2p.idun.core

import kotlinx.io.Buffer

fun encodeInto(toEnc: Map<String, Any>, buffer: Buffer) {
    encodeMap(toEnc, buffer)
}

private fun encodeInternal(o: Any, buffer: Buffer) {
    if (o is Map<*, *>) {
        encodeMap(o as Map<String, Any>, buffer)
        return
    }

    if (o is List<*>) {
        encodeList(o as List<Any>, buffer)
        return
    }

    if (o is String) {
        encodeString(o, buffer)
        return
    }

    if (o is ByteArray) {
        encodeInt(o.size, buffer, ':'.code.toByte())
        buffer.write(o)
        return
    }

    if (o is Int) {
        buffer.writeByte('i'.code.toByte())
        encodeInt(o, buffer, 'e'.code.toByte())
        return
    }

    if (o is Long) {
        buffer.writeByte('i'.code.toByte())
        encodeLong(o, buffer)
        return
    }

    if (o is StringWriter) {
        encodeInt(o.length(), buffer, ':'.code.toByte())
        o.writeTo(buffer)
        return
    }

    throw RuntimeException("unknown object to encode $o")
}


private fun encodeList(l: List<Any>, target: Buffer) {
    target.writeByte('l'.code.toByte())
    l.forEach { o: Any -> encodeInternal(o, target) }
    target.writeByte('e'.code.toByte())
}

private fun encodeString(str: String, target: Buffer) {
    encodeInt(str.length, target, ':'.code.toByte())
    str2buf(str, target)
}

private fun encodeMap(map: Map<String, Any>, target: Buffer) {
    target.writeByte('d'.code.toByte())
    val str = map.entries.sortedWith(java.util.Map.Entry.comparingByKey())

    str.forEach { e: Map.Entry<String, Any> ->
        encodeString(e.key, target)
        encodeInternal(e.value, target)
    }
    target.writeByte('e'.code.toByte())
}

private fun encodeInt(value: Int, target: Buffer, terminator: Byte) {
    var number = value
    if (number == Int.MIN_VALUE) target.write(MIN_INT)
    else {
        if (number < 0) {
            target.writeByte('-'.code.toByte())
            number = -number
        }

        var numChars = 10
        var probe = 10
        for (i in 1..9) {
            if (number < probe) {
                numChars = i
                break
            }
            probe *= 10
        }

        val out = ByteArray(numChars)
        val pos = out.size


        for (i in 1..numChars) {
            val reduced = number / 10
            val remainder = number - (reduced * 10)
            out[pos - i] = ('0'.code + remainder).toByte()
            number = reduced
        }

        target.write(out)
    }
    target.writeByte(terminator)
}

private fun encodeLong(value: Long, target: Buffer) {
    str2buf(value.toString(), target)
    target.writeByte('e'.code.toByte())
}

interface StringWriter {
    fun length(): Int

    fun writeTo(buf: Buffer)
}

private val MIN_INT: ByteArray = Int.MIN_VALUE.toString().toByteArray(Charsets.ISO_8859_1)