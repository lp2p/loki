package tech.lp2p.idun.core

import kotlin.math.min

object Arrays {

    fun compareUnsigned(a: ByteArray, b: ByteArray): Int {
        return compareUnsignedFallback(a, b)
    }

    fun mismatch(a: ByteArray, b: ByteArray): Int {
        return mismatchFallback(a, b)
    }

    private fun compareUnsignedFallback(a: ByteArray, b: ByteArray): Int {
        val minLength = min(a.size.toDouble(), b.size.toDouble()).toInt()
        run {
            var i = 0
            while (i + 7 < minLength) {
                val la = a[i].toULong() shl 56 or (
                        a[i + 1].toULong() shl 48) or (
                        a[i + 2].toULong() shl 40) or (
                        a[i + 3].toULong() shl 32) or (
                        a[i + 4].toULong() shl 24) or (
                        a[i + 5].toULong() shl 16) or (
                        a[i + 6].toULong() shl 8) or
                        a[i + 7].toULong()
                val lb = b[i].toULong() shl 56 or (
                        b[i + 1].toULong() shl 48) or (
                        b[i + 2].toULong() shl 40) or (
                        b[i + 3].toULong() shl 32) or (
                        b[i + 4].toULong() shl 24) or (
                        b[i + 5].toULong() shl 16) or (
                        b[i + 6].toULong() shl 8) or
                        b[i + 7].toULong()

                if (la != lb) return la.compareTo(lb)

                i += 8
            }
        }


        for (i in 0 until minLength) {
            val ia = a[i].toULong()
            val ib = b[i].toULong()
            if (ia != ib) return ia.compareTo(ib)
        }

        return a.size - b.size
    }


    private fun mismatchFallback(a: ByteArray, b: ByteArray): Int {
        val min = min(a.size, b.size)
        for (i in 0 until min) {
            if (a[i] != b[i]) return i
        }

        return if (a.size == b.size) -1 else min
    }
}
