package tech.lp2p.idun.core

import tech.lp2p.idun.messages.FindNodeRequest
import tech.lp2p.idun.messages.GetPeersRequest
import tech.lp2p.idun.messages.Message
import tech.lp2p.idun.messages.PingRequest
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.TimeUnit
import kotlin.math.max

/**
 * Two-step detection.
 *
 *
 * 1. we store responses that deviated form their expected ID. we can't ban for this since
 * the expectation (supplied by external nodes) can be incorrect. but we can use it to
 * fix up future expectations
 * 2. we do an active lookup or wait for a second call to complete to see whether the
 * node really changes ID
 */
class IDMismatchDetector(private val node: Node) {
    private val underObservation: MutableMap<Address, ObservationEntry> =
        ConcurrentHashMap()
    private val merged: MutableMap<Address, Long> = ConcurrentHashMap()
    private val activeLookups: MutableSet<Call> = ConcurrentHashMap.newKeySet()

    fun clear() {
        underObservation.clear()
        merged.clear()
        activeLookups.clear()
    }

    fun add(toInspect: Call) {
        if (toInspect.state() != CallState.RESPONDED) return

        updateExisting(toInspect)

        if (toInspect.expectedID == null) return

        if (!toInspect.matchesExpectedID()) {
            passiveObservation(toInspect)
            activeCheck(toInspect)
        }
    }

    private fun updateExisting(newCall: Call) {
        underObservation.computeIfPresent(newCall.request.address) { _: Address?, obs: ObservationEntry ->
            val newId = newCall.response!!.id
            if (obs.state == State.OBSERVING_PASSIVELY &&
                !obs.lastObservedId.contentEquals(newId)
            ) {
                val newObservation = ObservationEntry(
                    newId,
                    State.CONFIRMED_INCONSISTENT_ID,
                    PASSIVE_ID_CHANGE_BAN_DURATION
                )

                return@computeIfPresent newObservation
            }
            obs
        }
    }

    private fun passiveObservation(suspect: Call) {
        val e = ObservationEntry(
            suspect.response!!.id, State.OBSERVING_PASSIVELY,
            System.currentTimeMillis() + OBSERVATION_PERIOD
        )


        // updateExisting() will take care of other cases
        underObservation.putIfAbsent(suspect.request.address, e)
    }

    private fun activeCheck(suspect: Call) {
        val confirmedID = suspect.response!!.id

        // only probe in 33% of the cases
        if (ThreadLocalRandom.current().nextInt(3) > 0) return

        val addr = suspect.request.address

        val currentEntry = underObservation[addr]

        if (currentEntry != null && System.currentTimeMillis() -
            currentEntry.lastActiveCheck < ACTIVE_CHECK_BACKOFF_INTERVAL
        ) {
            return
        }

        val rnd = ThreadLocalRandom.current().nextInt(3)

        val mtid = createRandomKey(TID_LENGTH)

        // some fake based on node id, some based on lookup, so probe different things
        val request = when (rnd % 3) {
            0 -> PingRequest(addr, node.nodeId, mtid)
            1 -> GetPeersRequest(
                addr, node.nodeId, mtid, createRandomKey(SHA1_HASH_LENGTH)
            )

            else -> FindNodeRequest(
                addr, node.nodeId, mtid, createRandomKey(SHA1_HASH_LENGTH)
            )
        }


        val probeCall = Call(request, confirmedID)

        probeCall.addListener(object : CallListener {
            override suspend fun onTimeout(call: Call) {
            }

            override fun onResponse(call: Call, rsp: Message) {
            }

            override suspend fun stateTransition(
                call: Call,
                previous: CallState,
                current: CallState
            ) {
                if (current == CallState.ERROR || current == CallState.RESPONDED || current == CallState.TIMEOUT) {
                    val now = System.currentTimeMillis()

                    underObservation.compute(call.request.address) { _: Address?, existingObservationEntry: ObservationEntry? ->

                        val lastActiveCheck = now
                        var state = State.OBSERVING_PASSIVELY
                        var expirationTime = now + ACTIVE_CHECK_BACKOFF_INTERVAL
                        var lastObservedId = confirmedID

                        if (current == CallState.RESPONDED) {
                            lastObservedId = call.response!!.id
                            if (!call.matchesExpectedID()) {
                                state = State.CONFIRMED_INCONSISTENT_ID
                                expirationTime = now + ACTIVE_ID_CHANGE_BAN_DURATION
                            }
                        }

                        if (existingObservationEntry != null) {
                            expirationTime = max(
                                expirationTime,
                                existingObservationEntry.expirationTime
                            ).toLong()
                            if (existingObservationEntry.state == State.CONFIRMED_INCONSISTENT_ID)
                                state = State.CONFIRMED_INCONSISTENT_ID
                        }
                        ObservationEntry(lastObservedId, state, expirationTime, lastActiveCheck)
                    }

                    activeLookups.remove(call)
                }
            }
        })

        if (activeLookups.add(probeCall)) node.doCall(probeCall)
    }

    /**
     * @param forExpectedId if null is passed only checks for known-inconsistent nodes,
     * otherwise it also checks whether the ID matches a recent observation
     */
    fun isIdInconsistencyExpected(addr: Address, forExpectedId: ByteArray?): Boolean {
        if (merged.containsKey(addr)) return true
        val e = underObservation[addr] ?: return false
        if (e.state == State.CONFIRMED_INCONSISTENT_ID) return true
        if (forExpectedId != null) return !e.lastObservedId.contentEquals(forExpectedId)
        return false
    }

}

private val OBSERVATION_PERIOD = TimeUnit.MINUTES.toMillis(15)
private val ACTIVE_ID_CHANGE_BAN_DURATION = TimeUnit.HOURS.toMillis(12)

// passive observations happen over larger time windows, so they might catch some "normal"
// ID changes (node restarts). swing a lighter ban-hammer for those cases
private val PASSIVE_ID_CHANGE_BAN_DURATION = TimeUnit.MINUTES.toMillis(40)
private val ACTIVE_CHECK_BACKOFF_INTERVAL = TimeUnit.MINUTES.toMillis(25)

internal enum class State {
    CONFIRMED_INCONSISTENT_ID,
    OBSERVING_PASSIVELY
}

internal data class ObservationEntry(
    val lastObservedId: ByteArray,
    val state: State,
    val expirationTime: Long,
    val lastActiveCheck: Long = 0
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ObservationEntry

        if (expirationTime != other.expirationTime) return false
        if (lastActiveCheck != other.lastActiveCheck) return false
        if (!lastObservedId.contentEquals(other.lastObservedId)) return false
        if (state != other.state) return false

        return true
    }

    override fun hashCode(): Int {
        var result = expirationTime.hashCode()
        result = 31 * result + lastActiveCheck.hashCode()
        result = 31 * result + lastObservedId.contentHashCode()
        result = 31 * result + state.hashCode()
        return result
    }


}