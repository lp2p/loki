package tech.lp2p.idun.core


abstract class Tokenizer(var reader: Reader) {
    private val stack = mutableListOf(Token())

    var lastDecodedNum: Long = 0
    private var stackIdx = 0

    abstract fun push(token: Token)

    abstract fun pop(token: Token)


    private fun push(t: TokenType, pos: Int) {
        val current = stack[stackIdx]
        if (current.expect() == DictState.ExpectKeyOrEnd && t != TokenType.PREFIXED_STRING) throw BDecodingException(
            "encountered " + t.toString() +
                    " at offset " + pos + " while expecting a dictionary key"
        )

        stackIdx++
        stack.add(Token())

        if (stackIdx >= MAX_STACK_SIZE) throw BDecodingException("nesting too deep")

        val newState = stack[stackIdx]
        newState.start = pos
        newState.type(t)
        if (t == TokenType.DICT) newState.expect(DictState.ExpectKeyOrEnd)
        push(newState)
    }

    private fun pop(pos: Int) {
        var current = stack[stackIdx]

        if (current.type() == TokenType.DICT && current.expect() == DictState.ExpectValue) throw BDecodingException(
            "encountered 'e' (offset: " + reader.position() + ") after dictionary key, expected a value"
        )

        current.end = pos
        pop(current)

        lastDecodedNum = -1

        current.reset()
        stackIdx--

        current = stack[stackIdx]

        when (current.expect()) {
            DictState.ExpectKeyOrEnd -> current.expect(DictState.ExpectValue)
            DictState.ExpectValue -> current.expect(DictState.ExpectKeyOrEnd)
            else -> {}
        }
    }

    fun getSlice(token: Token): ByteArray {
        val oldPos = reader.position()
        reader.position(token.start)
        val slice = reader.getByteArray(token.end - token.start)
        reader.position(oldPos)
        return slice
    }


    private fun decodeString() {
        var length = parseNum(reader, ':'.code.toByte())
        if (length < 0) length = 0
        push(TokenType.STRING, reader.position())

        if (length > reader.remaining()) throw BDecodingException(
            ("string (offset: " + reader.position() + " + length: "
                    + length + ") points beyond end of message (length: " + reader.limit() + ")")
        )
        reader.position((reader.position() + length).toInt())
        pop(reader.position())
    }

    fun tokenize() {
        while (reader.remaining() > 0) {
            val pos = reader.position()

            when (val current = reader.getByte()) {
                'd'.code.toByte() -> push(TokenType.DICT, pos)
                'i'.code.toByte() -> {
                    push(TokenType.LONG, pos)
                    lastDecodedNum = parseNum(reader, 'e'.code.toByte())
                    pop(reader.position())
                }

                'l'.code.toByte() -> push(TokenType.LIST, pos)
                'e'.code.toByte() -> pop(reader.position())
                '-'.code.toByte(), '0'.code.toByte(), '1'.code.toByte(), '2'.code.toByte(),
                '3'.code.toByte(), '4'.code.toByte(), '5'.code.toByte(), '6'.code.toByte(),
                '7'.code.toByte(), '8'.code.toByte(), '9'.code.toByte() -> {
                    push(TokenType.PREFIXED_STRING, pos)
                    reader.position(pos)
                    decodeString()
                    pop(reader.position())
                }

                else -> {
                    val b = StringBuilder()
                    toHex(byteArrayOf(current), b, 1)
                    throw BDecodingException(
                        "unexpected character 0x" +
                                b + " at offset " + (reader.position() - 1)
                    )
                }
            }

            if (stackIdx <= 0) break
        }

        if (stackIdx != 0) throw BDecodingException(
            "reached end of data with unterminated " +
                    "lists/dictionaries on the stack"
        )
    }
}

const val MAX_STACK_SIZE = 256


class Token internal constructor() {
    var start: Int = 0
    var end: Int = 0
    var state: Byte = 0
    private var dictExpect: Byte = 0

    init {
        reset()
    }

    fun reset() {
        start = -1
        end = -1
        state = -1
        expect(DictState.NoExpectation)
    }

    fun type(): TokenType {
        return tokenEnums[state.toInt()]
    }

    fun type(t: TokenType) {
        state = t.ordinal.toByte()
    }

    fun expect(): DictState {
        return stateEnums[dictExpect.toInt()]
    }

    fun expect(d: DictState) {
        dictExpect = d.ordinal.toByte()
    }


    override fun toString(): String {
        var st = "Token: " + type()
        if (type() == TokenType.DICT) st += " " + expect()
        st += " [$start,$end]"

        return st
    }
}

enum class TokenType {
    LIST, DICT, PREFIXED_STRING, STRING, LONG
}

enum class DictState {
    NoExpectation,
    ExpectKeyOrEnd,
    ExpectValue
}


class BDecodingException internal constructor(msg: String?) : RuntimeException(msg)


private val tokenEnums = TokenType.entries.toTypedArray()
private val stateEnums = DictState.entries.toTypedArray()

private fun parseNum(reader: Reader, terminator: Byte): Long {
    var result: Long = 0
    var neg = false

    if (reader.remaining() < 1) throw BDecodingException(
        "end of message reached while decoding a" +
                " number/string length prefix. offset:" + reader.position()
    )
    var current = reader.getByte()

    if (current == '-'.code.toByte()) {
        neg = true
        if (reader.remaining() < 1) throw BDecodingException(
            "end of message reached while decoding " +
                    "a number/string length prefix. offset:" + reader.position()
        )
        current = reader.getByte()
    }

    var iter = 0

    // do-while since we expect at least one digit
    do {
        // do zero-check on 2nd character, since 0 itself is a valid length
        if (iter > 0 && result == 0L) throw BDecodingException(
            "encountered a leading zero at " +
                    "offset " + (reader.position() - 1) + " while decoding a number/string length prefix"
        )

        if (current < '0'.code.toByte() || current > '9'.code.toByte()) {
            val b = StringBuilder()
            toHex(byteArrayOf(current), b, 1)
            throw BDecodingException(
                "encountered invalid character 0x" + b + " (offset:" + (reader.position() - 1) + ") " +
                        "while decoding a number/string length prefix, expected 0-9 or " + Char(
                    terminator.toUShort()
                )
            )
        }


        val digit = current - '0'.code.toByte()

        result *= 10
        result += digit.toLong()

        if (reader.remaining() < 1) throw BDecodingException(
            "end of message reached while " +
                    "decoding a number/string length prefix. offset:" + reader.position()
        )
        current = reader.getByte()

        iter++
    } while (current != terminator)

    if (neg) result *= -1
    return result
}
