package tech.lp2p.idun.core

import java.net.InetAddress
import java.net.InetSocketAddress


class Peer(val item: ByteArray) : Comparable<Peer> {

    init {
        require(
            !(item.size != ADDRESS_LENGTH_IPV6
                    && item.size != ADDRESS_LENGTH_IPV4)
        )
        { "byte array length does not match ipv6 raw InetAddress+Port length" }
    }


    val inetAddress: InetAddress
        get() {
            if (item.size == ADDRESS_LENGTH_IPV4) return InetAddress.getByAddress(
                item.copyOf(4)
            )
            if (item.size == ADDRESS_LENGTH_IPV6) return InetAddress.getByAddress(
                item.copyOf(16)
            )
            throw IllegalStateException()
        }

    private fun toSocketAddress(): InetSocketAddress {
        return InetSocketAddress(addressAsString, port)
    }

    private val addressAsString: String
        get() = checkNotNull(inetAddress.hostAddress)

    override fun equals(other: Any?): Boolean {
        if (other is Peer) {
            if (other.item.size != item.size) return false
            for (i in 0 until item.size - 2) if (other.item[i] != item[i]) return false
            return true
        }
        return false
    }

    override fun hashCode(): Int {
        return item.copyOf(item.size - 2).contentHashCode()
    }


    override fun toString(): String {
        val b = StringBuilder(25)
        b.append(" addr:")
        b.append(toSocketAddress())
        return b.toString()
    }

    val port: Int
        get() {
            if (item.size == ADDRESS_LENGTH_IPV4) return (item[4]
                .toInt() and 0xFF) shl 8 or (item[5].toInt() and 0xFF)
            if (item.size == ADDRESS_LENGTH_IPV6) return (item[16]
                .toInt() and 0xFF) shl 8 or (item[17].toInt() and 0xFF)
            return 0
        }


    override fun compareTo(other: Peer): Int {
        return Arrays.compareUnsigned(item, other.item)
    }
}
