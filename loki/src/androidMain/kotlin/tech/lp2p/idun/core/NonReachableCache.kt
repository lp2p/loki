package tech.lp2p.idun.core

import java.util.concurrent.ConcurrentHashMap

class NonReachableCache {
    private val map = ConcurrentHashMap<Address, CacheEntry>()

    fun clear() {
        map.clear()
    }

    fun onCallFinished(c: Call) {
        val addr = c.request.address
        val state = c.state()

        when (state) {
            CallState.RESPONDED -> map.computeIfPresent(addr) { _: Address, oldEntry: CacheEntry ->
                if (oldEntry.failures <= 1) return@computeIfPresent null
                // multiplicative decrease
                CacheEntry(oldEntry.created, oldEntry.failures shr 1)
            }

            CallState.TIMEOUT -> {
                val newEntry = CacheEntry(System.currentTimeMillis(), 1)
                map.merge(addr, newEntry) { _: CacheEntry?, oldEntry: CacheEntry ->
                    CacheEntry(oldEntry.created, oldEntry.failures + 1)
                }
            }

            else -> {}
        }
    }

    fun getFailures(addr: Address): Int {
        return map[addr]?.failures ?: 0

    }

    internal data class CacheEntry(val created: Long, val failures: Int)

}
