package tech.lp2p.idun.core


fun decode(reader: Reader): Map<String, Any> {
    val root = decodeInternal(reader)
    if (root is Map<*, *>) {
        return root as Map<String, Any>
    }
    throw BDecodingException("expected dictionary as root object")
}

fun longGet(map: Map<in String, *>, key: String): Long? {
    val data = map[key]
    if (data is Long) {
        return data
    }
    return null
}

fun typedGet(map: Map<in String, *>, key: String): ByteArray? {
    val data = map[key]
    if (data is ByteArray) {
        return data
    }
    return null
}

private fun decodeInternal(reader: Reader): Any? {
    val consumer = Consumer(reader)
    consumer.tokenize()
    return consumer.result[0]
}

private class Consumer(reader: Reader) : Tokenizer(reader) {
    val result = mutableListOf<Any>()

    var keyPendingInsert: String? = null

    var depth: Int = 0

    override fun push(token: Token) {
        when (token.type()) {
            TokenType.DICT -> {
                val map: Any = mutableMapOf<String, Any>()
                putObject(map)
                result.add(depth, map)
                depth++
            }

            TokenType.LIST -> {
                val list: Any = mutableListOf<Any>()
                putObject(list)
                result.add(depth, list)
                depth++
            }

            TokenType.LONG, TokenType.STRING, TokenType.PREFIXED_STRING -> {
                return
            }
        }

    }


    fun putObject(value: Any) {
        var o = value
        if (depth == 0) {
            result.add(0, o)
            return
        }

        val container = result[depth - 1]


        if (container is HashMap<*, *>) {
            if (keyPendingInsert != null) {

                if ((container as HashMap<String?, Any?>).put(
                        keyPendingInsert,
                        o
                    ) != null
                ) throw BDecodingException("duplicate key found in dictionary")
                keyPendingInsert = null
            } else {
                keyPendingInsert = (o as ByteArray).toString(Charsets.ISO_8859_1)
            }
        } else if (container is ArrayList<*>) {
            (container as ArrayList<Any?>).add(o)
        } else {
            throw RuntimeException("this should not happen")
        }
    }

    override fun pop(token: Token) {
        when (token.type()) {
            TokenType.DICT, TokenType.LIST -> depth--
            TokenType.LONG -> putObject(lastDecodedNum)
            TokenType.STRING -> putObject(getSlice(token))
            TokenType.PREFIXED_STRING -> {
            }

        }
    }
}

